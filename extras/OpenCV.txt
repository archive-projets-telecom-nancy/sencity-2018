cmake
    -D CMAKE_BUILD_TYPE=Release
    -D CMAKE_INSTALL_PREFIX=/usr/local
    -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules
    -D WITH_V4L=ON
    -D WITH_LIBV4L=ON
    -D WITH_FFMPEG=1
    -D BUILD_opencv_java=OFF
    ..
