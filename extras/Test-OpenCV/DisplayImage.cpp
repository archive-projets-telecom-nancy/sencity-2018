#include <cstdio>
#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>

using namespace cv;

int main(int argc, char** argv )
{
    ocl::setUseOpenCL(false);
    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened()) {  // check if we succeeded
        return -1;
    }

    namedWindow("edges",1);

    Mat frame;

    for(;;)
    {      
        cap >> frame;
        imshow("edges", frame);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}