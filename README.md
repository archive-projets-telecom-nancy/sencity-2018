# Sencity - 360° computer vision

## Prérequis

* Compilateur C++17
* OpenCV 4+
* ffmpeg
* MathGL

## Compilation

```bash
cd src/
cmake .
cmake --build . --target all
```

## Utilisation

`Sencity [-i|--input vidéo ou webcam] [-p|--preset préréglage]`

préréglages possibles :
* `ricoh_theta`