//
// Created by symeon on 05/06/18.
//

#include <cstdio>
#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>

#define CVUI_IMPLEMENTATION
#include "Toolbox/cvui.h"

#include "VideoCapture/VideoCaptureProcessings/RicohThetaS.h"
#include "VideoCapture/VideoCaptureProcessings/VideoFile.h"
#include "VideoCapture/FrameProcessings/ChannelsExplorer.h"
#include "VideoCapture/FrameProcessings/FramePipes/Composition.h"
#include "VideoCapture/FrameProcessings/FramePipeComposition.h"

#include "Toolbox/cxxopts.hpp"
#include "VideoCapture/VideoCaptureProcessings/AutoRicohTheta.h"


int main(int argc, const char** argv ) {

    std::map<std::string,std::function<std::unique_ptr<VideoCaptureProcessing>()>> presets;
    presets.insert(std::make_pair("ricoh_theta",[]{return std::make_unique<AutoRicohTheta>();}));

    try {
        cxxopts::Options options("Sencity", "360° computer vision");
        options.add_options()
                ("i,input", "Video file or input stream to be used",cxxopts::value<std::string>()->default_value("/dev/video0"))
                ("p,preset", "Use a predefined set of effects",cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        cv::VideoCapture cap;

        cap.open(result["i"].as<std::string>());

        if(!cap.isOpened()) {  // check if we succeeded
            throw std::runtime_error("Impossible de se connecter à la caméra");
        }

        if (result.count("preset")) {
            std::string preset_name = result["preset"].as<std::string>();
            if (presets.count(preset_name)) {
                presets[preset_name]()->process(cap);
            } else {
                throw std::runtime_error("Unknown preset : '"+preset_name+"'");
            }
        } else {
            FramePipeComposition fpc = FramePipeComposition();
            VideoFile vf(fpc);
            vf.process(cap);
        }

    } catch (const cxxopts::OptionException& e) {
        std::cout << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
