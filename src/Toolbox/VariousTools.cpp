//
// Created by symeon on 21/06/18.
//

#include <opencv2/opencv.hpp>
#include <utility>
#include <opencv/cv.h>
#include "VariousTools.h"
#include "../Transforms/AffineTransform.h"


void polyfit(const cv::Mat& src_x, const cv::Mat& src_y, cv::Mat& dst, int order) {

    // taken from :
    // https://github.com/kipr/opencv/blob/master/modules/contrib/src/polyfit.cpp

    CV_Assert(src_x.rows>0);
    CV_Assert(src_y.rows>0);
    CV_Assert(src_x.cols==1);
    CV_Assert(src_y.cols==1);
    CV_Assert(dst.cols==1);
    CV_Assert(dst.rows==(order+1));
    CV_Assert(order>=1);


    cv::Mat X;
    X = cv::Mat::zeros(src_x.rows, order+1,CV_32FC1);
    cv::Mat copy;
    for(int i = 0; i <=order;i++)
    {
        copy = src_x.clone();
        pow(copy,i,copy);
        cv::Mat M1 = X.col(i);
        copy.col(0).copyTo(M1);
    }
    cv::Mat X_t, X_inv;
    transpose(X,X_t);
    cv::Mat temp = X_t*X;
    cv::Mat temp2;
    invert (temp,temp2);
    cv::Mat temp3 = temp2*X_t;
    cv::Mat W = temp3*src_y;
    W.copyTo(dst);
}

cv::Point2d transformPoint(double x, double y, AffineTransform xaf, AffineTransform yaf) {
    return cv::Point2d(
            xaf.transform(x),
            yaf.transform(y)
    );
}

void drawPoints(cv::Mat &frame, std::array<cv::Point2f, 4> points, int selectedPoint, cv::Scalar pointColor,
                cv::Scalar selectedPointColor) {
    for (int i = 0; i < 4; ++i) {
        if (i == selectedPoint) {
            cv::circle(frame,points.at(i),4,selectedPointColor,1,CV_AA);
            drawTarget(frame,points.at(i),selectedPointColor);
        } else {
            cv::circle(frame,points.at(i),4,pointColor,1,CV_AA);
        }
    }
}

void drawQuadAndPoints(cv::Mat frame, std::array<cv::Point2f, 4> points, int selectedPoint, cv::Scalar lineColor,
                       cv::Scalar pointColor, cv::Scalar selectedPointColor) {

    cv::line(frame,points.at(0),points.at(1),lineColor,1,CV_AA);
    cv::line(frame,points.at(1),points.at(3),lineColor,1,CV_AA);
    cv::line(frame,points.at(3),points.at(2),lineColor,1,CV_AA);
    cv::line(frame,points.at(2),points.at(0),lineColor,1,CV_AA);

    for (int i = 0; i < 4; ++i) {
        if (i == selectedPoint) {
            cv::circle(frame,points.at(i),4,selectedPointColor,1,CV_AA);
            drawTarget(frame,points.at(i),selectedPointColor);
        } else {
            cv::circle(frame,points.at(i),4,pointColor,1,CV_AA);
        }
    }
}

void drawTarget(cv::Mat frame, cv::Point point, cv::Scalar color) {
    for (int j = 0; j < 4; ++j) {
        cv::line(
                frame,
                cv::Point2f(
                        static_cast<float>(point.x - 6 * -1 * pow(-1, j / 2)),
                        static_cast<float>(point.y - 6 * -1 * pow(-1, j % 2))
                ),
                cv::Point2f(
                        static_cast<float>(point.x - 10 * -1 * pow(-1, j / 2)),
                        static_cast<float>(point.y - 10 * -1 * pow(-1, j % 2))
                ),
                color,
                1,
                CV_AA
        );
    }
}

void drawPoly(cv::Mat frame, const cv::Mat poly, const cv::Scalar color, int thickness) {
    std::function<double(double)> polynomial = [&poly](double y){
        cv::MatConstIterator_<float> it, end;
        int i = 0;
        double res = 0;
        for (it = poly.begin<float>(), end = poly.end<float>(); it != end; ++it, ++i) {
            res += *it * pow(y,i);
        }
        return res;
    };
    double y = 0;
    cv::Point2d before(polynomial(y),y);
    cv::Point2d after;
    double intermediateLength = frame.rows / 100.0;
    for (int i = 0; i < 100 ; ++i) {
        y += intermediateLength;
        after = cv::Point2d(polynomial(y), y);
        cv::line(frame,before,after,color,thickness,CV_AA);
        before = after;
    }
}
