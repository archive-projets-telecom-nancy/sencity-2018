//
// Created by symeon on 15/06/18.
//

#include <opencv2/opencv.hpp>

#include "LaplacianKernel.h"
#include "../Transforms/AffineTransform.h"

cv::Mat LaplacianKernel::horizontalLaplacianKernel(int radius) {

    int side_length = 2*radius+1;

    cv::Mat kernel = cv::Mat::zeros(side_length,side_length,cv::DataType<double>::type);

    AffineTransform iToFunctionInput(0,side_length-1,-2,2);

    double x;
    double somme = 0;
    for (int i = 0; i < side_length-1; ++i) {
        x = iToFunctionInput.transform(i);
        kernel.at<double>(radius,i) = laplacian(x,0.5);
        somme += laplacian(x,0.5);
    }

    kernel /= somme;
    //std::cout << "kernel : " << kernel << std::endl;

    return kernel;
}

double LaplacianKernel::laplacian(double x, double sigma) {
    double front_factor = -1.0 * (1.0/(M_PI*pow(sigma,4)));
    double middle_factor = (1-(pow(x,2)/(2*pow(sigma,2))));
    double rear_factor = exp(-1.0 * ((pow(x,2))/(2*pow(sigma,2))));

    return front_factor*middle_factor*rear_factor;
}
