//
// Created by symeon on 21/06/18.
//

#ifndef SENCITY_HALP_H
#define SENCITY_HALP_H

#include "../Transforms/AffineTransform.h"

void polyfit(const cv::Mat& src_x, const cv::Mat& src_y, cv::Mat& dst, int order);
cv::Point2d transformPoint(double x, double y, AffineTransform xaf, AffineTransform yaf);
void drawPoints(cv::Mat &frame, std::array<cv::Point2f, 4> points, int selectedPoint, cv::Scalar pointColor,
                cv::Scalar selectedPointColor);
void drawQuadAndPoints(cv::Mat frame, std::array<cv::Point2f, 4> points, int selectedPoint, cv::Scalar lineColor,
                       cv::Scalar pointColor, cv::Scalar selectedPointColor);
void drawTarget(cv::Mat frame, cv::Point point, cv::Scalar color);
void drawPoly(cv::Mat frame, cv::Mat poly, cv::Scalar color, int thickness);
#endif //SENCITY_HALP_H
