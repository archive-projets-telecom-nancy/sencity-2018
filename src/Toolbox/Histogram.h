//
// Created by symeon on 13/07/18.
//

#ifndef SENCITY_HISTOGRAM_H
#define SENCITY_HISTOGRAM_H


#include "Graph.h"

class Histogram : public Graph {
public:
    Histogram(std::string name,std::string color);

    ~Histogram() override;

    void justDraw(std::vector<float> data);
    void draw(std::vector<float> data) override;
    void justDraw(cv::Mat data);
    void draw(cv::Mat data) override;
    void drawWithPeaks(cv::Mat data, std::vector<int> peaks, int radius);
    void show();
    void drawGUI() override;

    std::string name;
    std::string color;

private:
    std::vector<float> vectorData;
    bool scaleFrozen;
    double lastMax;
};


#endif //SENCITY_HISTOGRAM_H
