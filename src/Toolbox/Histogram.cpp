//
// Created by symeon on 13/07/18.
//

#include <opencv2/imgproc.hpp>
#include <opencv/cv.hpp>
#include <utility>
#include "Histogram.h"
#include "cvui.h"

Histogram::Histogram(std::string name, std::string color):
    Graph(),
    name(std::move(name)),
    color(std::move(color)),
    scaleFrozen(false),
    lastMax(1.0),
    vectorData()
{

}

Histogram::~Histogram() = default;

void Histogram::justDraw(std::vector<float> data) {
    vectorData = std::vector<float>(data);

    graphData = mglData(vectorData.size(),1,1);
    graphData.Set(vectorData);

    theGraph.ClearFrame();
    theGraph.SetRange('x',0,data.size()-1);
    theGraph.AddLegend(name.c_str(),":rC");

    if (not scaleFrozen) {
        theGraph.SetRange('y',graphData);
    }

    theGraph.Plot(graphData,color.c_str());
    theGraph.Axis();
}

void Histogram::draw(std::vector<float> data) {
    justDraw(data);
    show();
}

void Histogram::drawGUI() {
    if (not scaleFrozen) {
        if(cvui::button("Freeze Scale")) {
            lastMax = *std::max_element(vectorData.begin(),vectorData.end())*1.2;
            scaleFrozen = true;
            theGraph.SetRange('y',0,lastMax);
        }
    } else {
        if(cvui::button("Unfreeze Scale")) {
            scaleFrozen = false;
        }
    }
}

void Histogram::justDraw(cv::Mat data) {
    justDraw(std::vector<float>(data.begin<float>(),data.end<float>()));
}

void Histogram::draw(cv::Mat data) {
    draw(std::vector<float>(data.begin<float>(),data.end<float>()));
}

void Histogram::show() {
    Image = cv::Mat(theGraph.GetHeight(),theGraph.GetWidth(),CV_8UC3,(void*) theGraph.GetRGB());
    cv::cvtColor(Image,Image,cv::COLOR_BGR2RGB);
    cv::imshow(name,Image);
}

void Histogram::drawWithPeaks(cv::Mat data, std::vector<int> peaks, int radius) {
    justDraw(data);
    for (int &peak : peaks) {
        theGraph.Puts(mglPoint(peak,vectorData[peak]*1.05,0),"\\downarrow");
        theGraph.Line(mglPoint(peak-radius,0,0),mglPoint(peak-radius,lastMax,0),"b=");
        theGraph.Line(mglPoint(peak+radius,0,0),mglPoint(peak+radius,lastMax,0),"b=");
    }
    show();
}
