//
// Created by symeon on 13/07/18.
//

#ifndef SENCITY_GRAPH_H
#define SENCITY_GRAPH_H

#include <mgl2/data.h>
#include <opencv2/core/mat.hpp>
#include <mgl2/mgl.h>

class Graph {
public:
    Graph();
    virtual ~Graph();
    virtual void draw(std::vector<float> data) = 0;
    virtual void draw(cv::Mat data) = 0;
    virtual void drawGUI() = 0;

protected:
    mglData graphData;
    mglGraph theGraph;
    cv::Mat Image;
};


#endif //SENCITY_GRAPH_H
