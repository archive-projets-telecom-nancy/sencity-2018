//
// Created by symeon on 15/06/18.
//

#ifndef SENCITY_LAPLACIANKERNEL_H
#define SENCITY_LAPLACIANKERNEL_H


#include <opencv2/core/mat.hpp>

class LaplacianKernel {
public:
    static cv::Mat horizontalLaplacianKernel(int radius);

    static double laplacian(double x, double sigma);
};


#endif //SENCITY_LAPLACIANKERNEL_H
