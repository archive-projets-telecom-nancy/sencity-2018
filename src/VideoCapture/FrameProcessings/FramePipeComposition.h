//
// Created by symeon on 09/07/18.
//

#ifndef SENCITY_FRAMEPIPECOMPOSITION_H
#define SENCITY_FRAMEPIPECOMPOSITION_H


#include "FrameProcessing.h"
#include "FramePipes/Composition.h"

class FramePipeComposition : public FrameProcessing {
public:
    explicit FramePipeComposition();

    void process(cv::Mat frame) override;
    void drawGUI() override;

    Composition cp;
    cv::Mat interface;
};


#endif //SENCITY_FRAMEPIPECOMPOSITION_H
