//
// Created by symeon on 11/07/18.
//

#ifndef SENCITY_SIMPLEFRAMEMIXER_H
#define SENCITY_SIMPLEFRAMEMIXER_H


#include <opencv2/core/mat.hpp>

class SimpleFrameMixer {
public:
    SimpleFrameMixer();

    virtual ~SimpleFrameMixer();

    virtual cv::Mat mix(cv::Mat A, cv::Mat B);
    virtual void drawGUI();
};


#endif //SENCITY_SIMPLEFRAMEMIXER_H
