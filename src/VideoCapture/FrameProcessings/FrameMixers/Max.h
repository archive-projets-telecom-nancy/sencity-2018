//
// Created by symeon on 11/07/18.
//

#ifndef SENCITY_MAX_H
#define SENCITY_MAX_H


#include "SimpleFrameMixer.h"

class Max : public SimpleFrameMixer {
public:
    Max();

    ~Max() override;

    cv::Mat mix(cv::Mat A, cv::Mat B) override;

    void drawGUI() override;
};


#endif //SENCITY_MAX_H
