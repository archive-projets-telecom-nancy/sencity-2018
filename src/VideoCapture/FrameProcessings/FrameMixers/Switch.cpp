//
// Created by symeon on 11/07/18.
//

#include "Switch.h"
#include "../../../Toolbox/cvui.h"

Switch::Switch() : outputA(true) {

}

Switch::~Switch() {

}

cv::Mat Switch::mix(cv::Mat A, cv::Mat B) {
    return (outputA ? A : B).clone();
}

void Switch::drawGUI() {
    if(outputA) {
        if(cvui::button("> A  ")) {
            outputA = false;
        }
    } else {
        if(cvui::button("  B <")) {
            outputA = true;
        }
    }
}
