//
// Created by symeon on 11/07/18.
//

#include "Max.h"


Max::Max() : SimpleFrameMixer() {

}

Max::~Max() {

}

cv::Mat Max::mix(cv::Mat A, cv::Mat B) {
    return cv::max(A, B);
}

void Max::drawGUI() {
    SimpleFrameMixer::drawGUI();
}
