//
// Created by symeon on 11/07/18.
//

#ifndef SENCITY_SWITCH_H
#define SENCITY_SWITCH_H


#include "SimpleFrameMixer.h"

class Switch : public SimpleFrameMixer {
public:
    Switch();
    ~Switch() override;

    cv::Mat mix(cv::Mat A, cv::Mat B) override;
    void drawGUI() override;

    bool outputA;
};


#endif //SENCITY_SWITCH_H
