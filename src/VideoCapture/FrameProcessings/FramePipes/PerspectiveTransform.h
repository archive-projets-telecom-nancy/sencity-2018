//
// Created by symeon on 04/07/18.
//

#ifndef SENCITY_PERSPECTIVETRANSFORM_H
#define SENCITY_PERSPECTIVETRANSFORM_H

#include <opencv/cv.hpp>
#include "SimplePipe.h"

class PerspectiveTransform : public SimplePipe {
public:
    explicit PerspectiveTransform();

    ~PerspectiveTransform() override;

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

    void updatePerspective();
    void guessQuadsFromLabda(cv::Mat mat);

    cv::Mat lastFrame;
    cv::Mat perspectiveTransformationMatrix;
    std::array<cv::Point2f,4> inputQuad;
    std::array<cv::Point2f,4> outputQuad;

    bool shouldGuessQuadsFromLabda;
    int selected_point;
    bool outputSelected;
};

void inputQuadMouseCallback(int event, int x, int y, int flags, void *userdata);
void outputQuadMouseCallback(int event, int x, int y, int flags, void *userdata);
int closestPointIndex(int x, int y, std::array<cv::Point2f, 4> array);


#endif //SENCITY_PERSPECTIVETRANSFORM_H
