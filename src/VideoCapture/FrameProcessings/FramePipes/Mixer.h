//
// Created by symeon on 09/07/18.
//

#ifndef SENCITY_MIXER_H
#define SENCITY_MIXER_H

#include <memory>
#include "SimplePipe.h"
#include "Composition.h"
#include "../FrameMixers/SimpleFrameMixer.h"

class Mixer : public SimplePipe {
public:
    Mixer();
    ~Mixer() override;

    Mixer(const Composition &A, const Composition &B);

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

    Composition A;
    Composition B;

    std::map<std::string,std::function<std::unique_ptr<SimpleFrameMixer>()>> allowedFrameMixers;

    struct {
        std::string name;
        std::unique_ptr<SimpleFrameMixer> frameMixer;
    } frameMixer;

    bool mixingModeFailed;
    bool outputMixerSelected;
};


#endif //SENCITY_MIXER_H
