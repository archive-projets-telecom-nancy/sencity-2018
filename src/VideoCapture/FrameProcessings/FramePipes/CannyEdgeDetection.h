//
// Created by symeon on 09/07/18.
//

#ifndef SENCITY_CANNYEDGEDETECTION_H
#define SENCITY_CANNYEDGEDETECTION_H


#include "SimplePipe.h"

class CannyEdgeDetection : public SimplePipe {
public:
    CannyEdgeDetection();
    ~CannyEdgeDetection() override;

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

    int lowerBound;
    int upperBound;
};


#endif //SENCITY_CANNYEDGEDETECTION_H
