//
// Created by symeon on 03/07/18.
//

#include "Composition.h"
#include <utility>
#include "ColorspaceConversion.h"
#include "Threshold.h"
#include "../../../Toolbox/cvui.h"
#include "PerspectiveTransform.h"
#include "Mixer.h"
#include "CannyEdgeDetection.h"
#include "ChannelSelector.h"
#include "PerspectiveProjection.h"
#include "EqualizeHistogram.h"
#include "RossKippenbrockLaneDetection.h"
#include "ReversiblePerspective.h"

Composition::Composition() :
        framePipeFactories(),
        steps(),
        numberOfSteps(0)
{
    framePipeFactories.insert(std::make_pair("Nothing",[]{return std::make_unique<SimplePipe>();}));
    framePipeFactories.insert(std::make_pair("Threshold",[]{return std::make_unique<Threshold>();}));
    framePipeFactories.insert(std::make_pair("Colorspace Conversion",[]{return std::make_unique<ColorspaceConversion>();}));
    framePipeFactories.insert(std::make_pair("Transform Perspective",[]{return std::make_unique<PerspectiveTransform>();}));
    framePipeFactories.insert(std::make_pair("Mixer",[]{return std::make_unique<Mixer>();}));
    framePipeFactories.insert(std::make_pair("Canny Edge Detection",[]{return std::make_unique<CannyEdgeDetection>();}));
    framePipeFactories.insert(std::make_pair("Channel Selector",[]{return std::make_unique<ChannelSelector>();}));
    framePipeFactories.insert(std::make_pair("Perspective Projection",[]{return std::make_unique<PerspectiveProjection>();}));
    framePipeFactories.insert(std::make_pair("Equalize Histogram",[]{return std::make_unique<EqualizeHistogram>();}));
    framePipeFactories.insert(std::make_pair("Ross-Kippenbrock Lane Detection",[]{return std::make_unique<RossKippenbrockLaneDetection>();}));
    framePipeFactories.insert(std::make_pair("Reversible Perspective Transform",[]{return std::make_unique<ReversiblePerspective>();}));
}

cv::Mat Composition::process(cv::Mat frame)  {
    cv::Mat res = frame.clone();
    for (auto& step : steps) {
        res = step.fp->process(res);
    }
    return res.clone();
}

void Composition::drawGUI() {

    if(cvui::counter(&this->numberOfSteps) <= 0) {
        this->numberOfSteps = 0;
    }

    if (numberOfSteps != steps.size()) {
        if (numberOfSteps > this->steps.size()) {
            while (this->steps.size() < numberOfSteps) {
                this->steps.emplace_back("Nothing");
            }
        } else {
            while (this->steps.size() > numberOfSteps) {
                this->steps.pop_back();
            }
        }
    }

    for (auto& elt : this->steps) {
        if (cvui::checkbox(elt.name, &elt.selected)) {
            for (auto &pipe : framePipeFactories) {
                if (cvui::button(pipe.first)) {
                    elt.name = pipe.first;
                    elt.fp = std::move(pipe.second());
                    elt.selected = false;
                }
            }
        } else {
            cvui::beginRow(-1, -1, 2);
            {
                cvui::rect(15, 0, 0xff000000);
                cvui::beginColumn(-1, -1, 2);
                elt.fp->drawGUI();
                cvui::endColumn();
            }
            cvui::endRow();
        }
        cvui::rect(100, 1, 0x707070);
    }
}

NamedFramePipe::NamedFramePipe(std::string string, std::unique_ptr<SimplePipe> unique_ptr, bool b):
    name(std::move(string)),
    fp(std::move(unique_ptr)),
    selected(b)
{}

NamedFramePipe &NamedFramePipe::operator=(const NamedFramePipe &n) {
    if (this != &n) {
        name = n.name;
        fp = std::make_unique<SimplePipe>(*(n.fp));
        selected = n.selected;
    }
    return *this;
}

NamedFramePipe::NamedFramePipe(const NamedFramePipe &n):
    name(n.name),
    fp(std::make_unique<SimplePipe>(*(n.fp))),
    selected(n.selected)
{}

NamedFramePipe::NamedFramePipe(const std::string &name) : name(name), fp(std::make_unique<SimplePipe>()), selected(false) {}
