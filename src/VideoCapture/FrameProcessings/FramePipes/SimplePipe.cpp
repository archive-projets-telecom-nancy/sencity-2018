//
// Created by symeon on 03/07/18.
//

#include <opencv2/core/mat.hpp>
#include "SimplePipe.h"

SimplePipe::SimplePipe() = default;

cv::Mat SimplePipe::process(cv::Mat mat) {
    return mat.clone();
}

void SimplePipe::drawGUI() {

}

SimplePipe::~SimplePipe() = default;

