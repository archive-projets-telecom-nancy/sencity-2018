//
// Created by symeon on 03/07/18.
//

#ifndef SENCITY_VIDEOPIPLINE_H
#define SENCITY_VIDEOPIPLINE_H

#include <opencv2/core/mat.hpp>

class SimplePipe {
public:
    SimplePipe();

    virtual ~SimplePipe();

    virtual cv::Mat process(cv::Mat);
    virtual void drawGUI();
};


#endif //SENCITY_VIDEOPIPLINE_H
