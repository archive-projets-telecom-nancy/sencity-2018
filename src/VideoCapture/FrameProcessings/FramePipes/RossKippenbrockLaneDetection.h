//
// Created by symeon on 12/07/18.
//

#ifndef SENCITY_ROSSKIPPENBROCKLANEDETECTION_H
#define SENCITY_ROSSKIPPENBROCKLANEDETECTION_H


#include <list>
#include "SimplePipe.h"
#include "../../../Toolbox/Histogram.h"

class RossKippenbrockLaneDetection : public SimplePipe {
public:
    RossKippenbrockLaneDetection();

    RossKippenbrockLaneDetection(bool showLanes, bool showInput);

    ~RossKippenbrockLaneDetection() override;

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

private:

    void updateWeightingCurve(cv::MatSize size);
    std::vector<int> findPeaks(cv::Mat histogram, int radius, int number, double threshold);
    void updateRectanglesAndPoints(cv::Mat mat, std::vector<int> peaks);
    int medianDrift(cv::Mat rectangleHistogram);
    int averageDrift(cv::Mat rectangleHistogram);
    std::list<cv::Point> findPoints(cv::Mat mat, int xShift, int yShift);

    Histogram laneGuess;
    double histogramPortion;
    bool useWeightedHistogram;
    cv::Mat weightingCurve;
    double sigma;
    int numberOfPeaks;
    int peakRadius;
    double peakThreshold;

    int numberOfRectangles;
    int rectangleWidth;
    bool useMedianInsteadOfAverage;
    std::vector<std::vector<cv::Rect>> rectangles;
    std::vector<std::list<cv::Point>> points;
    std::vector<cv::Mat> laneCurves;

    bool recognitionFailed;
    bool unsuitableMatType;
    bool showHistogram;
    bool showRectangles;
    bool showPoints;
    bool showLanes;
    bool showInput;

    bool showHistogramOptions;
    bool showPeaksOptions;
};


#endif //SENCITY_ROSSKIPPENBROCKLANEDETECTION_H
