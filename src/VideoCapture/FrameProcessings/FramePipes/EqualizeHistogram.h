//
// Created by symeon on 12/07/18.
//

#ifndef SENCITY_NORMALIZEHISTOGRAM_H
#define SENCITY_NORMALIZEHISTOGRAM_H


#include "SimplePipe.h"

class EqualizeHistogram : public SimplePipe {
public:
    EqualizeHistogram();

    ~EqualizeHistogram() override;

    cv::Mat process(cv::Mat mat) override;

    void drawGUI() override;
};


#endif //SENCITY_NORMALIZEHISTOGRAM_H
