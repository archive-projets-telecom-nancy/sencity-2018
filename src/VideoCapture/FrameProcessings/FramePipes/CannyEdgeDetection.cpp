//
// Created by symeon on 09/07/18.
//

#include <opencv2/imgproc.hpp>
#include "CannyEdgeDetection.h"
#include "../../../Toolbox/cvui.h"

CannyEdgeDetection::CannyEdgeDetection():
        lowerBound(50),
        upperBound(100)
{

}

CannyEdgeDetection::~CannyEdgeDetection() {

}

cv::Mat CannyEdgeDetection::process(cv::Mat mat) {
    cv::Mat res;
    cv::Canny(mat,res,lowerBound,upperBound);
    return res.clone();
}

void CannyEdgeDetection::drawGUI() {
    cvui::text("Lower Bound");
    cvui::trackbar(175,&lowerBound,0,255);
    cvui::text("Upper Bound");
    cvui::trackbar(175,&upperBound,0,255);
}
