//
// Created by symeon on 09/07/18.
//

#include <opencv2/core.hpp>
#include "Mixer.h"
#include "../../../Toolbox/cvui.h"
#include "../FrameMixers/Switch.h"
#include "../FrameMixers/Max.h"

Mixer::Mixer() :
        A(),
        B(),
        mixingModeFailed(false),
        outputMixerSelected(false),
        allowedFrameMixers(),
        frameMixer()
{
    allowedFrameMixers.insert(std::make_pair("Switch",[]{return std::make_unique<Switch>();}));
    allowedFrameMixers.insert(std::make_pair("Max",[]{return std::make_unique<Max>();}));
    frameMixer = {"Switch",std::move(allowedFrameMixers["Switch"]())};
}

Mixer::~Mixer() {
}

cv::Mat Mixer::process(cv::Mat mat) {
    cv::Mat resA = A.process(mat);
    cv::Mat resB = B.process(mat);
    cv::Mat res(resA.size(),CV_8UC3);
    if (resA.size != resB.size) {
        std::cout << "resA.size : " << resA.size << " resB.size : " << resB.size << std::endl;
        cv::resize(resB,resB,resA.size());
    }
    try {
        res = frameMixer.frameMixer->mix(resA,resB);
        this->mixingModeFailed = false;
    } catch(cv::Exception const& e) {
        if (not this->mixingModeFailed) {
            std::cout << "mixing the two layers failed : " << e.what() << std::endl;
            this->mixingModeFailed = true;
        }
        return mat.clone();
    }
    return res.clone();
}

void Mixer::drawGUI() {
    cvui::beginRow(-1,-1,2);
    {
        cvui::beginColumn(-1,-1,2);
        {
            A.drawGUI();
        }
        cvui::endColumn();
        cvui::rect(1,100,0x707070);
        cvui::beginColumn(-1,-1,2);
        {
            B.drawGUI();
        }
        cvui::endColumn();
    }
    cvui::endRow();
    if (cvui::checkbox(frameMixer.name,&outputMixerSelected)) {
        for (auto& elt : this->allowedFrameMixers) {
            if (cvui::button(elt.first)) {
                frameMixer = {elt.first,elt.second()};
                outputMixerSelected = false;
            }
        }
    } else {
        frameMixer.frameMixer->drawGUI();
    }
}

Mixer::Mixer(const Composition &A, const Composition &B) : Mixer() {
    this->A = A;
    this->B = B;
}
