//
// Created by symeon on 04/07/18.
//

#include <opencv2/imgproc.hpp>
#include "PerspectiveTransform.h"
#include "../../../Toolbox/cvui.h"
#include "../../../Toolbox/VariousTools.h"

#define INPUT_WINNAME "Perspective Input"
#define OUTPUT_WINNAME "Perspective Output"

int closestPointIndex(int x, int y, std::array<cv::Point2f, 4> array);

PerspectiveTransform::PerspectiveTransform():
    lastFrame(),
    perspectiveTransformationMatrix(),
    inputQuad({cv::Point2f(0,0),cv::Point2f(0,0),cv::Point2f(0,0),cv::Point2f(0,0)}),
    outputQuad({cv::Point2f(0,0),cv::Point2f(0,0),cv::Point2f(0,0),cv::Point2f(0,0)}),
    shouldGuessQuadsFromLabda(true),
    selected_point(0),
    outputSelected(true)
{
    double l[3][3] = {
            {-0.2859031241394075, -1.720632783574553, 418.4926532235851},
            {-1.817990202823694e-15, -2.573779872096934, 530.1986536519689},
            {-1.951563910473908e-18, -0.005284416021541566, 1}
    };
    perspectiveTransformationMatrix = cv::Mat(3,3,CV_64F,(void*) l).clone();
}

cv::Mat PerspectiveTransform::process(cv::Mat frame) {
    if (frame.channels() == 1) {
        cv::cvtColor(frame,lastFrame,cv::COLOR_GRAY2BGR);
    } else {
        lastFrame = frame.clone();
    }
    if (shouldGuessQuadsFromLabda) {
        guessQuadsFromLabda(frame);
        shouldGuessQuadsFromLabda = false;
    }
    cv::Mat res;
    cv::Mat output = cv::Mat(frame.rows, frame.cols, frame.type());
    cv::warpPerspective(frame, res, this->perspectiveTransformationMatrix, output.size());
    return res.clone();
}

void PerspectiveTransform::drawGUI() {
    if(lastFrame.total() > 0) {
        if(cvui::button("Adjust Perspective")) {
            updatePerspective();
        }
    }
    else {
        cvui::text("No input !",0.4,0xC41A00);
    }
}

void PerspectiveTransform::updatePerspective() {

    std::array<cv::Point2f,4> oldInputQuad(inputQuad);
    std::array<cv::Point2f,4> oldOutputQuad(outputQuad);

    cv::Mat output = cv::Mat::zeros( lastFrame.rows, lastFrame.cols, lastFrame.type() );

    cv::Mat lambda;

    cv::namedWindow(INPUT_WINNAME,1);
    cv::namedWindow(OUTPUT_WINNAME,1);

    cv::moveWindow(INPUT_WINNAME,40,40);
    cv::moveWindow(OUTPUT_WINNAME,40+700,40);

    cv::setMouseCallback(INPUT_WINNAME,inputQuadMouseCallback, (void*) this);
    cv::setMouseCallback(OUTPUT_WINNAME,outputQuadMouseCallback, (void*) this);

    cv::Mat input_frame = lastFrame.clone();

    selected_point = 0;
    outputSelected = true;
    int key;
    while(true) {
        input_frame = lastFrame.clone();
        lambda = cv::getPerspectiveTransform( inputQuad, outputQuad );
        cv::warpPerspective(input_frame,output,lambda,output.size());

        if (outputSelected) {
            drawPoints(output, outputQuad, selected_point, cv::Scalar(255, 200, 5), cv::Scalar(255, 255, 255));
        } else {
            drawQuadAndPoints(input_frame, inputQuad, selected_point, cv::Scalar(255, 233, 175),
                              cv::Scalar(255, 200, 5), cv::Scalar(255, 255, 255));
        }


        cv::imshow("Perspective Input", input_frame);
        cv::imshow("Perspective Output",output);
        if ((key = cv::waitKey(1)) >= 0) {
            switch (key) {

                case 13: // ENTER
                    std::cout << "lambda : " << lambda << std::endl;
                    perspectiveTransformationMatrix = lambda.clone();
                    cv::destroyWindow("Perspective Input");
                    cv::destroyWindow("Perspective Output");
                    return;
                case 27: // ECHAP
                    inputQuad = std::array<cv::Point2f,4>(oldInputQuad);
                    outputQuad = std::array<cv::Point2f,4>(oldOutputQuad);
                    cv::destroyWindow("Perspective Input");
                    cv::destroyWindow("Perspective Output");
                    return;
                    // touches 1 à 4 du pavé numérique
                case 177:
                case 178:
                case 179:
                case 180:
                    selected_point = key-177;
                    break;
                case 97: // A
                    outputSelected = false;
                    break;
                case 98: // B
                    outputSelected = true;
                    break;

                case 81: // GAUCHE
                    (outputSelected ? outputQuad : inputQuad)[selected_point].x -= 1;
                    break;
                case 82: // HAUT
                    (outputSelected ? outputQuad : inputQuad)[selected_point].y -= 1;
                    break;
                case 83: // DROITE
                    (outputSelected ? outputQuad : inputQuad)[selected_point].x += 1;
                    break;
                case 84: // BAS
                    (outputSelected ? outputQuad : inputQuad)[selected_point].y += 1;
                    break;

                default:
                    std::cout << "key pressed :" << key << std::endl;
            }
        }
    }

}

void PerspectiveTransform::guessQuadsFromLabda(cv::Mat frame) {
    outputQuad = {
            cv::Point2f(0,0),
            cv::Point2f(frame.cols-1,0),
            cv::Point2f(static_cast<float>(frame.cols * 3 / 8.0), frame.rows - 1),
            cv::Point2f(static_cast<float>(frame.cols * 5 / 8.0), frame.rows - 1)
    };
    inputQuad = {
            cv::Point2f(0, static_cast<float>(frame.rows / 2.0)),
            cv::Point2f(frame.cols-1, static_cast<float>(frame.rows / 2.0)),
            cv::Point2f(0,frame.rows-1),
            cv::Point2f(frame.cols-1,frame.rows-1)
    };
    cv::perspectiveTransform(outputQuad,inputQuad,perspectiveTransformationMatrix.inv());
}

PerspectiveTransform::~PerspectiveTransform() = default;

void inputQuadMouseCallback(int event, int x, int y, int flags, void *userdata) {

    auto pt = static_cast<PerspectiveTransform*>(userdata);

    switch (event) {
        case cv::EVENT_LBUTTONDOWN:
            if (pt->outputSelected) {
                pt->outputSelected = false;
            } else {
                pt->selected_point = closestPointIndex(x, y, pt->inputQuad);
            }
            break;
        case cv::EVENT_MOUSEMOVE:
            if (flags & cv::EVENT_FLAG_LBUTTON) {
                pt->inputQuad[pt->selected_point].x = x;
                pt->inputQuad[pt->selected_point].y = y;
            }
            break;
        default:
            break;
    }
}

void outputQuadMouseCallback(int event, int x, int y, int flags, void *userdata) {

    auto pt = static_cast<PerspectiveTransform*>(userdata);

    switch (event) {
        case cv::EVENT_LBUTTONDOWN:
            if (not pt->outputSelected) {
                pt->outputSelected = true;
                return;
            } else {
                pt->selected_point = closestPointIndex(x, y, pt->outputQuad);
                return;
            }
            break;
        case cv::EVENT_MOUSEMOVE:
            if (flags & cv::EVENT_FLAG_LBUTTON) {
                pt->outputQuad[pt->selected_point].x = x;
                pt->outputQuad[pt->selected_point].y = y;
            }
            break;
        default:
            break;
    }
}

int closestPointIndex(int x, int y, std::array<cv::Point2f, 4> array) {
    double distance_min = pow(x-array[0].x,2)+pow(y-array[0].y,2);
    int indice_min = 0;
    for (int i = 1; i <4; ++i) {
        double distance = pow(x-array[i].x,2)+pow(y-array[i].y,2);
        if (distance < distance_min) {
            distance_min = distance;
            indice_min = i;
        }
    }
    return indice_min;
}
