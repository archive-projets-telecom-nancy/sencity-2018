//
// Created by symeon on 12/07/18.
//

#include <opencv2/imgproc.hpp>
#include "EqualizeHistogram.h"

EqualizeHistogram::EqualizeHistogram() = default;

EqualizeHistogram::~EqualizeHistogram() = default;

cv::Mat EqualizeHistogram::process(cv::Mat mat) {
    cv::Mat res = mat.clone();
    switch (mat.channels()) {
        case 3:
            cv::cvtColor(res,res,cv::COLOR_BGR2GRAY);
        case 1:
            cv::equalizeHist(res,res);
        default:
            break;
    }
    return res.clone();
}

void EqualizeHistogram::drawGUI() {
    SimplePipe::drawGUI();
}
