//
// Created by symeon on 11/06/18.
//

#ifndef SENCITY_PERSPECTIVEPROJECTION_H
#define SENCITY_PERSPECTIVEPROJECTION_H


#include <opencv2/opencv.hpp>
#include "SimplePipe.h"

class PerspectiveProjection : public SimplePipe {
public:
    PerspectiveProjection();
    ~PerspectiveProjection() override;

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;


    PerspectiveProjection(int output_width, int output_height, double horizontalFOV, int input_width=1280, int input_height=640, int input_crop=37);

    cv::Mat warp(cv::Mat input_image);

private:

    cv::Size outputSize;
    double horizontalFOV;

    cv::Size inputSize;
    int inputCrop;

    double extraAzimuth;
    double extraInclinaison;
    double extraRoulis;

    cv::Rect input_zone_A;
    cv::Rect input_zone_B;

    cv::Mat map_x;
    cv::Mat map_y;
    void updateMaps();
    void updateInputZones();
    double zeroCenteredModulo(double angle);
};


#endif //SENCITY_PERSPECTIVETRANSFORM_H
