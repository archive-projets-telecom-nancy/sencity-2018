//
// Created by symeon on 11/07/18.
//

#ifndef SENCITY_CHANNELSELECTOR_H
#define SENCITY_CHANNELSELECTOR_H


#include "SimplePipe.h"

class ChannelSelector : public SimplePipe {
public:
    ChannelSelector();
    ~ChannelSelector() override;

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

    int clamp(int value);

    int numberOfChannels;
    int selectedChannel;
};


#endif //SENCITY_CHANNELSELECTOR_H
