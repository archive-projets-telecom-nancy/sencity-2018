//
// Created by symeon on 12/07/18.
//

#include <opencv2/core.hpp>
#include <queue>
#include <functional>
#include <iostream>
#include <mgl2/mgl.h>
#include <opencv/cv.h>
#include "RossKippenbrockLaneDetection.h"
#include "../../../Transforms/AffineTransform.h"
#include "../../../Toolbox/VariousTools.h"
#include "../../../Toolbox/cvui.h"

RossKippenbrockLaneDetection::RossKippenbrockLaneDetection() :
    laneGuess("Lane Guess","r"),
    histogramPortion(0.5),
    useWeightedHistogram(true),
    sigma(1.5),
    numberOfPeaks(2),
    peakRadius(40),
    peakThreshold(50),
    numberOfRectangles(10),
    rectangleWidth(60),
    useMedianInsteadOfAverage(false),
    recognitionFailed(false),
    unsuitableMatType(false),
    rectangles(),
    points(),
    showHistogram(false),
    showRectangles(false),
    showPoints(false),
    showLanes(true),
    showInput(true),
    showHistogramOptions(false),
    showPeaksOptions(false)
{

}

RossKippenbrockLaneDetection::~RossKippenbrockLaneDetection() = default;

cv::Mat RossKippenbrockLaneDetection::process(cv::Mat mat) {

    cv::Mat laneMarks(mat.rows,mat.cols,CV_8UC3,cv::Scalar(0,0,0));
    if (showInput) {
        if (mat.channels() == 1) {
            cv::cvtColor(mat,laneMarks,CV_GRAY2BGR);
        } else {
            laneMarks = mat.clone();
        }
    }

    recognitionFailed = false;

    // HISTOGRAM
    cv::Rect roi(0, static_cast<int>(mat.size().height*(1-histogramPortion)), mat.size().width, static_cast<int>(mat.size().height*histogramPortion));
    cv::Mat histogramRegion = mat(roi);
    cv::Mat histogram;
    cv::reduce(histogramRegion,histogram,0,cv::REDUCE_SUM,CV_32FC1);
    if (useWeightedHistogram) {
        if (weightingCurve.size != histogram.size) {
            updateWeightingCurve(histogram.size);
        }
        histogram.mul(weightingCurve);
    }

    // PEAKS
    auto peaks = findPeaks(histogram, peakRadius, numberOfPeaks, peakThreshold);
    if (showHistogram) {
        laneGuess.drawWithPeaks(histogram,peaks,peakRadius);
    }
    if (peaks.size() != numberOfPeaks) {
        if (laneMarks.channels() == 1) {
            cv::cvtColor(laneMarks,laneMarks,cv::COLOR_GRAY2BGR);
        }
        cv::putText(laneMarks,"Recognition Failed",cv::Point(30,30),cv::FONT_HERSHEY_SIMPLEX,1.0,cv::Scalar(0,26,196));
        return laneMarks.clone();
    }

    // BOXES
    updateRectanglesAndPoints(mat,peaks);

    if (showRectangles) {
        for (auto const& setOfRectangles : rectangles) {
            for (auto const& rectangle : setOfRectangles) {
                cv::rectangle(laneMarks,rectangle,cv::Scalar(255, 200, 5),1,CV_AA);
            }
        }
    }

    if (showPoints) {
        cv::Mat negation_mask(laneMarks.rows,laneMarks.cols,laneMarks.type(),cv::Scalar(0,0,0));
        for (auto const& pointStack : points) {
            for (auto const& point : pointStack)  {
                cv::circle(negation_mask,point,4,cv::Scalar(255, 255, 255),1,CV_AA);
            }
        }
        laneMarks = cv::abs(laneMarks-negation_mask);
    }

    laneCurves.clear();
    for (auto const& list : points) {
        std::vector<float> xVect;
        std::vector<float> yVect;
        xVect.reserve(list.size());
        yVect.reserve(list.size());
        std::transform(list.begin(),list.end(),std::back_inserter(xVect),[](cv::Point p){return static_cast<float>(p.x);});
        std::transform(list.begin(),list.end(),std::back_inserter(yVect),[](cv::Point p){return static_cast<float>(p.y);});
        cv::Mat x(static_cast<int>(list.size()), 1, cv::DataType<float>::type, static_cast<void*>(xVect.data()));
        cv::Mat y(static_cast<int>(list.size()), 1, cv::DataType<float>::type, static_cast<void*>(yVect.data()));
        cv::Mat laneCurve(3,1,cv::DataType<float>::type);
        polyfit(y,x,laneCurve,2);
        laneCurves.push_back(laneCurve);
    }

    if (showLanes) {
        for (auto const& lane: laneCurves) {
            drawPoly(laneMarks,lane,cv::Scalar(255, 200, 5),20);
        }
    }

    return laneMarks.clone();
}

void RossKippenbrockLaneDetection::drawGUI() {
    if (cvui::checkbox("Histogram",&showHistogramOptions)) {
        if (cvui::checkbox("Show Histogram", &showHistogram)) {
            laneGuess.drawGUI();
        }
        cvui::text("Height Proportion", 0.3);
        cvui::trackbar(200, &histogramPortion, 0.0, 1.0);
        if (cvui::checkbox("Use Weighted Histogram", &useWeightedHistogram)) {
            cvui::text("Sigma", 0.3);
            cvui::trackbar(200, &sigma, 0.0, 5.0);
        }
    }

    if (cvui::checkbox("Peaks",&showPeaksOptions)) {
        cvui::beginRow();
        {
            cvui::beginColumn();
            {
                cvui::text("Number Of Peaks", 0.3);
                if (cvui::counter(&numberOfPeaks) <= 0) {
                    numberOfPeaks = 0;
                }
            }
            cvui::endColumn();
            cvui::beginColumn();
            {
                cvui::text("Peak Search Radius", 0.3);
                if (cvui::counter(&peakRadius) <= 1) {
                    peakRadius = 1;
                }
            }
            cvui::endColumn();
        }
        cvui::endRow();
        cvui::text("Peak Threshold", 0.3);
        cvui::trackbar(200, &peakThreshold, 0.0, 255.0);
    }
    cvui::text("Peaks");
    {

    }
    cvui::text("Boxes / Points / Lanes");
    {
        cvui::checkbox("Show Boxes",&showRectangles);
        cvui::beginRow();
        {
            cvui::beginColumn();
            {
                cvui::text("Number of Boxes",0.3);
                if (cvui::counter(&numberOfRectangles) <= 0) {
                    numberOfRectangles = 0;
                }
            }
            cvui::endColumn();
            cvui::beginColumn();
            {
                cvui::text("Boxes Width",0.3);
                if (cvui::counter(&rectangleWidth) <= 1) {
                    rectangleWidth = 1;
                }
            }
            cvui::endColumn();
        }
        cvui::endRow();
        cvui::checkbox("Use Median Instead Of Average",&useMedianInsteadOfAverage);
        cvui::checkbox("Show Points",&showPoints);
        cvui::checkbox("Show Lanes",&showLanes);
        cvui::checkbox("Show Input",&showInput);
    }
}

void RossKippenbrockLaneDetection::updateWeightingCurve(cv::MatSize size) {

    weightingCurve = cv::Mat(size(),CV_32FC1);
    cv::MatIterator_<float> it, end;
    int i = 0;
    int width = size().width-1;
    for (it = weightingCurve.begin<float>(), end = weightingCurve.end<float>(); it != end; ++it) {
        *it = static_cast<float>(exp(-1 * pow((sigma * i / width) - 1, 2)));
    }

}

std::vector<int> RossKippenbrockLaneDetection::findPeaks(cv::Mat histogram, int radius, int number, double threshold) {

    std::priority_queue<int, std::vector<int>, std::function<bool(int,int)>>
            pre_res([histogram](int a, int b){
                        return histogram.at<float>(0,a) < histogram.at<float>(0,b);
                    });

    std::vector<int> res;

    double max_value;
    int max_delta;
    int x;
    cv::MatIterator_<float> it, end;
    int i = 0;
    for (it = histogram.begin<float>(), end = histogram.end<float>(); it != end; ++i, ++it) {
        if (*it > threshold) {
            max_value = 0;
            max_delta = -1;
            for (int delta = -radius; delta < radius + 1; ++delta) {
                x = std::max(0, i + delta);
                x = std::min(x, histogram.size().width);
                if (histogram.at<float>(0,x) > max_value) {
                    max_value = histogram.at<float>(0,x);
                    max_delta = delta;
                }
            }
            if (max_delta == 0) {
                pre_res.push(i);
            }
        }
    }

    while (not pre_res.empty() and res.size() < number) {
        res.push_back(pre_res.top());
        pre_res.pop();
    }

    return res;

}

void RossKippenbrockLaneDetection::updateRectanglesAndPoints(cv::Mat mat, std::vector<int> peaks) {

    std::function<int(int)> clampX = [mat](int x){return std::min(std::max(0,x),mat.cols-1);};
    std::function<int(int)> clampY = [mat](int y){return std::min(std::max(0,y),mat.rows-1);};

    rectangles.clear();
    points.clear();
    double rectangleHeight = static_cast<double>(mat.rows) / numberOfRectangles;
    for (auto const& peak : peaks) {
        int drift = 0;
        std::vector<cv::Rect> rectangleStack;
        std::list<cv::Point> pointList;
        for (int i = numberOfRectangles-1; i >= 0; --i) {

            int topleftY = clampY(static_cast<int>(i*rectangleHeight));
            int topleftX = clampX(static_cast<int>(peak-rectangleWidth/2.0+drift));
            int bottomRightY = clampY(static_cast<int>((i+1)*rectangleHeight)-1);
            int bottomRightX = clampX(static_cast<int>(peak+rectangleWidth/2.0+drift));

            cv::Rect rectangle(cv::Point(topleftX,topleftY),cv::Point(bottomRightX,bottomRightY));
            rectangleStack.push_back(rectangle);

            cv::Mat extract = mat(rectangle);
            if (extract.channels() != 1) {
                cv::cvtColor(extract,extract,cv::COLOR_BGR2GRAY);
            }
            cv::Mat redux;
            cv::reduce(extract,redux,0,cv::REDUCE_SUM,CV_32F);
            double new_drift;
            if (useMedianInsteadOfAverage) {
                new_drift = medianDrift(redux);
            } else {
                new_drift = averageDrift(redux);
            }
            drift += new_drift;
            pointList.emplace_back(topleftX+rectangleWidth/2.0+new_drift,
                                          static_cast<int>(topleftY + rectangleHeight / 2));
        }
        rectangles.push_back(rectangleStack);
        points.push_back(pointList);
    }
}

int RossKippenbrockLaneDetection::medianDrift(cv::Mat rectangleHistogram) {
    double valeur_mediane = cv::sum(rectangleHistogram)[0];
    int i = 0;
    double somme = 0;

    if (rectangleHistogram.type() != cv::DataType<float>::type) {
        std::ostringstream s;
        s << "Unexpected cv::Mat type in median drift calculation : " << rectangleHistogram.type();
        throw std::logic_error(s.str());
    }

    cv::MatIterator_<float> it, end;
    for (it = rectangleHistogram.begin<float>(), end = rectangleHistogram.end<float>(); it != end; ++it, ++i) {
        somme += *it;
        if (somme > valeur_mediane) {
            return static_cast<int>(i - rectangleHistogram.size().width / 2.0);
        }
    }
    if (i == 0) {
        throw std::runtime_error("zero pixels seen during average drift calculation");
    }
    throw std::runtime_error("no median found");
}

int RossKippenbrockLaneDetection::averageDrift(cv::Mat rectangleHistogram) {
    double weightedSum = 0;
    double sumOfWeights = 0;
    int i = 0;

    if (rectangleHistogram.type() != cv::DataType<float>::type) {
        std::ostringstream s;
        s << "Unexpected cv::Mat type in average drift calculation : " << rectangleHistogram.type();
        throw std::logic_error(s.str());
    }

    cv::MatIterator_<float> it, end;
    for (it = rectangleHistogram.begin<float>(), end = rectangleHistogram.end<float>(); it != end; ++it, ++i) {
        sumOfWeights += *it;
        weightedSum += *it*i;
    }
    if (i == 0) {
        throw std::runtime_error("zero pixels seen during average drift calculation");
    }
    if (std::abs(sumOfWeights) < 1e-7) {
        return 0;
    }
    return static_cast<int>(weightedSum/sumOfWeights - rectangleHistogram.size().width / 2.0);
}

std::list<cv::Point> RossKippenbrockLaneDetection::findPoints(cv::Mat mat, int xShift, int yShift) {
    std::list<cv::Point> res;
    int i = 0;

    if (mat.type() != cv::DataType<uint8_t>::type) {
        std::ostringstream s;
        s << "Unexpected cv::Mat type in point finding : " << mat.type();
        throw std::logic_error(s.str());
    }

    cv::MatIterator_<uint8_t> it,end;
    std::function<cv::Point(int)> makePoint = [&mat,xShift,yShift](int i){return cv::Point(i%mat.size().width+xShift,i/mat.size().width+yShift);};
    for (it = mat.begin<uint8_t>(), end = mat.end<uint8_t>(); it != end; ++it, ++i) {
        if (*it > 240.0) {
            res.push_back(makePoint(i));
        }
    }
    if (i == 0) {
        throw std::runtime_error("zero pixels seen during point finding");
    }
    return res;
}

RossKippenbrockLaneDetection::RossKippenbrockLaneDetection(bool showLanes, bool showInput) :
        RossKippenbrockLaneDetection::RossKippenbrockLaneDetection()
{
    this->showLanes = showLanes;
    this->showInput = showInput;
}
