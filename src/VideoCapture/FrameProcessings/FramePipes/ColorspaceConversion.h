//
// Created by symeon on 03/07/18.
//

#ifndef SENCITY_COLORSPACECONVERSION_H
#define SENCITY_COLORSPACECONVERSION_H

#include <opencv2/opencv.hpp>

#include "SimplePipe.h"

class ColorspaceConversion : public SimplePipe {
public:
    ColorspaceConversion();

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

    cv::ColorConversionCodes code;
    bool showConversions;

    std::map<cv::ColorConversionCodes ,std::string> allowedConversions;
};


#endif //SENCITY_COLORSPACECONVERSION_H
