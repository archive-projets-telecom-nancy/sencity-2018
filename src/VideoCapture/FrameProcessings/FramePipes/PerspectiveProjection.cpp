//
// Created by symeon on 11/06/18.
//

#include <cmath>

#include "PerspectiveProjection.h"
#include "../../../Transforms/AffineTransform.h"
#include "../../../Toolbox/cvui.h"

PerspectiveProjection::PerspectiveProjection(int output_width, int output_height, double horizontalFOV, int input_width,
                                           int input_height, int input_crop) {
    this->outputSize = {output_width,output_height};
    this->horizontalFOV = horizontalFOV;
    this->inputSize = {input_width,input_height};
    this->inputCrop = input_crop;

    extraAzimuth = 0;
    extraInclinaison = 0;

    updateInputZones();
    updateMaps();

}

PerspectiveProjection::PerspectiveProjection():
    outputSize(640,360),
    horizontalFOV(100),
    inputSize(-1,-1),
    inputCrop(37),
    extraAzimuth(0),
    extraInclinaison(0),
    extraRoulis(0)
{

}

PerspectiveProjection::~PerspectiveProjection() = default;

cv::Mat PerspectiveProjection::process(cv::Mat mat) {
    if (mat.size() != inputSize) {
        inputSize = mat.size();
        updateInputZones();
        updateMaps();
    }
    return warp(mat).clone();
}

void PerspectiveProjection::drawGUI() {
    cvui::text("Horizontal F.O.V.");
    if(cvui::trackbar(300,&horizontalFOV,0.0,180.0)) {
        updateMaps();
    }
    cvui::beginRow(-1,-1,2);
    {
        cvui::beginColumn(-1,-1,2);
        {
            cvui::text("Azimuth");
            if(cvui::trackbar(150,&extraAzimuth,-M_PI,M_PI)) {
                updateMaps();
            }
        }
        cvui::endColumn();
        cvui::beginColumn(-1,-1,2);
        {
            cvui::text("Inclinaison");
            if (cvui::trackbar(150,&extraInclinaison,-M_PI/2.0,M_PI/2)) {
                updateMaps();
            }
        }
        cvui::endColumn();

    }
    cvui::endRow();
    cvui::text("Roulis");
    if (cvui::trackbar(300,&extraRoulis,-M_PI,M_PI)) {
        updateMaps();
    }

}

void PerspectiveProjection::updateMaps() {

    map_x.create(outputSize.height,outputSize.width,cv::DataType<float>::type);
    map_y.create(outputSize.height,outputSize.width,cv::DataType<float>::type);

    AffineTransform degToRad(0,360,0,2*M_PI);
    double horizontalFOVRads = degToRad.transform(horizontalFOV);
    double horizontalScreenSize = 2*tan(horizontalFOVRads/2);

    AffineTransform OutputXToU(0,outputSize.width-1,-horizontalScreenSize/2,horizontalScreenSize/2);
    AffineTransform OutputYToV = AffineTransform(-OutputXToU.getCoeff()).matching(outputSize.height/2.0,0);

    AffineTransform xToInputX_A(-1,1,input_zone_A.x,input_zone_A.x+input_zone_A.width);
    AffineTransform yToInputY_A(-1,1,input_zone_A.y+input_zone_A.height,input_zone_A.y);

    AffineTransform xToInputX_B(-1,1,input_zone_B.x,input_zone_B.x+input_zone_B.width);
    AffineTransform yToInputY_B(-1,1,input_zone_B.y+input_zone_B.height,input_zone_B.y);

    double u,v,w;
    double rayon,inclinaison,azimuth;
    double Px, Pz;
    double rayon2, angle;
    double xA,yA;

    for (int x = 0; x < outputSize.width; ++x) {
        for (int y = 0; y < outputSize.height; ++y) {

            u = OutputXToU.transform(x);
            v = OutputYToV.transform(y);
            w = 1;

            rayon = sqrt(u*u + v*v + w*w);
            inclinaison = acos(v/rayon)+extraInclinaison;
            azimuth = zeroCenteredModulo(atan2(-w,-u)+extraAzimuth);

            Px = sin(inclinaison)*cos(azimuth) * -1.0; //je sais pas pourquoi il faut inverser au final ...
            //Py = sin(phi)*sin(theta);
            Pz = cos(inclinaison);

            rayon2 = sqrt(Px*Px+Pz*Pz);
            angle = atan2(Pz,Px)-M_PI/2+extraRoulis;

            xA = rayon2*cos(angle);
            yA = rayon2*sin(angle);

            if (azimuth > 0) {
                //cadran A
                map_x.at<float>(y,x) = static_cast<float>(xToInputX_A.transform(-xA));
                map_y.at<float>(y,x) = static_cast<float>(yToInputY_A.transform(yA));
            } else {
                //cadran B
                map_x.at<float>(y,x) = static_cast<float>(xToInputX_B.transform(xA));
                map_y.at<float>(y,x) = static_cast<float>(yToInputY_B.transform(yA));
            }

        }
    }

}

void PerspectiveProjection::updateInputZones() {
    this->input_zone_A = cv::Rect(inputCrop,inputCrop,inputSize.height-2*inputCrop,inputSize.height-2*inputCrop);
    this->input_zone_B = cv::Rect(inputSize.width/2+inputCrop,inputCrop,inputSize.height-2*inputCrop,inputSize.height-2*inputCrop);
}

cv::Mat PerspectiveProjection::warp(cv::Mat input_image) {
    cv::Mat output_image = cv::Mat(outputSize.height, outputSize.width, CV_8UC3, cv::Scalar(0, 0, 0));
    remap(input_image, output_image, map_x, map_y, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
    return output_image.clone();
}

double PerspectiveProjection::zeroCenteredModulo(double angle) {
    double angle_mod = remainder(angle+M_PI,2*M_PI);
    if (angle_mod < 0) {
        angle_mod += 2*M_PI;
    }
    return angle_mod-M_PI;
}
