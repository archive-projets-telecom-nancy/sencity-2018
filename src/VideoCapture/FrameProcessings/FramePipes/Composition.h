//
// Created by symeon on 03/07/18.
//

#ifndef SENCITY_FRAMEPIPELINE_H
#define SENCITY_FRAMEPIPELINE_H


#include <functional>
#include <memory>
#include <map>
#include "../FrameProcessing.h"
#include "SimplePipe.h"

struct NamedFramePipe {
    NamedFramePipe(std::string string, std::unique_ptr<SimplePipe> unique_ptr, bool b);

    NamedFramePipe(const std::string &name);

    NamedFramePipe(const NamedFramePipe& n);

    NamedFramePipe &operator=(const NamedFramePipe &n);

    std::string name;
    std::unique_ptr<SimplePipe> fp;
    bool selected;
};

class Composition : public SimplePipe {
public:
    explicit Composition();

    cv::Mat process(cv::Mat frame) override;
    void drawGUI() override;

    std::map<std::string,std::function<std::unique_ptr<SimplePipe>()>> framePipeFactories;
    std::vector<NamedFramePipe> steps;
    int numberOfSteps;

};


#endif //SENCITY_FRAMEPIPELINE_H
