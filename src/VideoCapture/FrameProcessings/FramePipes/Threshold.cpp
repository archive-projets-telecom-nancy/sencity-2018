//
// Created by symeon on 03/07/18.
//

#include "Threshold.h"
#include "../../../Toolbox/cvui.h"

Threshold::Threshold():
        threshold(127),
        maxval(255),
        type(cv::THRESH_BINARY),
        showTypes(false)
{
    ThresholdTypes = {
            {cv::THRESH_BINARY,"Binary"},
            {cv::THRESH_BINARY_INV,"Inverse Binary"},
            {cv::THRESH_TRUNC,"Truncate"},
            {cv::THRESH_TOZERO,"Clip to Zero"},
            {cv::THRESH_TOZERO_INV,"Inverse Clip to Zero"}
    };
}

cv::Mat Threshold::process(cv::Mat mat) {
    cv::Mat res;
    cv::threshold(mat,res,this->threshold,this->maxval,this->type);
    return res.clone();
}

void Threshold::drawGUI() {
    cvui::trackbar(175,&threshold,0.0,255.0);
    if(cvui::checkbox(ThresholdTypes.at(type),&this->showTypes)) {
        for (auto& elt : ThresholdTypes) {
            if(cvui::button(elt.second)) {
                this->type = elt.first;
                this->showTypes = false;
            }
        }
    }
}

Threshold::Threshold(double threshold) : Threshold() {
    this->threshold = threshold;
}

