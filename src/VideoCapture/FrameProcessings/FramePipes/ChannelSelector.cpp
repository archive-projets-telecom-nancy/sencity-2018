//
// Created by symeon on 11/07/18.
//

#include "ChannelSelector.h"
#include "../../../Toolbox/cvui.h"

ChannelSelector::ChannelSelector() :
    numberOfChannels(-1),
    selectedChannel(-1)
{

}

ChannelSelector::~ChannelSelector() {

}

cv::Mat ChannelSelector::process(cv::Mat mat) {
    cv::Mat res;
    numberOfChannels = mat.channels();
    selectedChannel = clamp(selectedChannel);
    cv::extractChannel(mat,res,selectedChannel);
    return res.clone();
}

int ChannelSelector::clamp(int value) {
    return std::min(std::max(0,value),numberOfChannels-1);
}

void ChannelSelector::drawGUI() {
    if (this->numberOfChannels != -1) {
        cvui::printf("(%d Channels)",numberOfChannels);
        int count = cvui::counter(&selectedChannel);
        if(count <= 0 or count >= numberOfChannels-1) {
            selectedChannel = clamp(selectedChannel);
        }
    } else {
        cvui::text("No input !",0.4,0xC41A00);
    }
}
