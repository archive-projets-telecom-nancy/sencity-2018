//
// Created by symeon on 03/07/18.
//

#ifndef SENCITY_THRESHOLD_H
#define SENCITY_THRESHOLD_H

#include <opencv2/opencv.hpp>
#include "SimplePipe.h"

class Threshold : public SimplePipe {
public:

    Threshold();

    Threshold(double threshold);

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;

    double threshold;
    double maxval;
    cv::ThresholdTypes type;
    bool showTypes;

    std::map<cv::ThresholdTypes,std::string> ThresholdTypes;
};


#endif //SENCITY_THRESHOLD_H
