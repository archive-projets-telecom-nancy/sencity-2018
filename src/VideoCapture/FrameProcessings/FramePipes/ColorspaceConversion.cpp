//
// Created by symeon on 03/07/18.
//

#include "ColorspaceConversion.h"
#include "../../../Toolbox/cvui.h"

ColorspaceConversion::ColorspaceConversion() {
    code = cv::COLOR_BGR2GRAY;
    showConversions = false;
    allowedConversions = {
            {cv::COLOR_BGR2GRAY,"BGR -> Gray"},
            {cv::COLOR_BGR2HSV,"BGR -> HSV"},
            {cv::COLOR_BGR2Lab,"BGR -> Lab"}
    };
}

cv::Mat ColorspaceConversion::process(cv::Mat mat) {
    cv::Mat res;
    cv::cvtColor(mat,res,code);
    return res.clone();
}

void ColorspaceConversion::drawGUI() {
    if (cvui::checkbox(allowedConversions.at(code),&showConversions)) {
        for (auto &elt : allowedConversions) {
            if (cvui::button(elt.second)) {
                code = elt.first;
                showConversions = false;
            }
        }
    }
}

