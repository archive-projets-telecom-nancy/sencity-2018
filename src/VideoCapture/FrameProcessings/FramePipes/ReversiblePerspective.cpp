//
// Created by symeon on 20/07/18.
//

#include "ReversiblePerspective.h"
#include "../../../Toolbox/cvui.h"

ReversiblePerspective::ReversiblePerspective():
        PerspectiveTransform(),
        composition(),
        showComposition(false),
        unwrapPerspective(true)
{

}

ReversiblePerspective::~ReversiblePerspective() = default;

cv::Mat ReversiblePerspective::process(cv::Mat mat) {
    cv::Mat res = PerspectiveTransform::process(mat);
    res = composition.process(res).clone();
    if (unwrapPerspective) {
        cv::warpPerspective(res,res,perspectiveTransformationMatrix.inv(),res.size());
    }
    return res.clone();
}

void ReversiblePerspective::drawGUI() {
    PerspectiveTransform::drawGUI();
    cvui::checkbox("Unwrap Perspective",&unwrapPerspective);
    if (cvui::checkbox("Composition",&showComposition)) {
        cvui::beginRow();
        {
            cvui::rect(5,0,0xFF0000);
            cvui::beginColumn(-1,-1,2);
            {
                composition.drawGUI();
            }
            cvui::endColumn();
        }
        cvui::endRow();
    }
}

ReversiblePerspective::ReversiblePerspective(const Composition &composition) : ReversiblePerspective() {
    this->composition = composition;
}
