//
// Created by symeon on 20/07/18.
//

#ifndef SENCITY_REVERSIBLEPERSPECTIVE_H
#define SENCITY_REVERSIBLEPERSPECTIVE_H


#include "PerspectiveTransform.h"
#include "Composition.h"

class ReversiblePerspective : public PerspectiveTransform {
public:
    explicit ReversiblePerspective();

    ReversiblePerspective(const Composition &composition);

    ~ReversiblePerspective() override;

    cv::Mat process(cv::Mat mat) override;
    void drawGUI() override;
    Composition composition;
    bool showComposition;
    bool unwrapPerspective;
};


#endif //SENCITY_REVERSIBLEPERSPECTIVE_H
