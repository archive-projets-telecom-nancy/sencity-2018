//
// Created by symeon on 18/06/18.
//

#ifndef SENCITY_MOTIONTEMPLATELANEDETECTION_H
#define SENCITY_MOTIONTEMPLATELANEDETECTION_H

#include <mgl2/mgl.h>
#include "FrameProcessing.h"

class MotionTemplateLaneDetection {
public:
    explicit MotionTemplateLaneDetection(int PsiThreshold, int delta);
    void withLambda(cv::Mat lambda);

    void process(cv::Mat frame);

    // Matrices et paramètres de traitement
    cv::Mat perspectiveMatrix;
    cv::Mat laplacianKernel;
    cv::Mat D;
    cv::Mat Psi;
    int PsiThreshold;
    int lmtDecay;
    int blobRadius;

    bool shouldThreshold;
    int silouhetteThreshold;

    bool shouldEqualizeHist;

    bool shouldBlur;
    int blurRadius;

    bool shouldDetectEdges;
    int cannyLowerBound;
    int cannyUpperBound;

    // Images et Images traitées
    cv::Mat laneSilhouette;
    cv::Mat laneBeforeThreshold;
    cv::Mat laneCannyEdgeDetection;
    cv::Mat LMT;

    // Graphiques
    cv::Mat laneIntesityImage;
    cv::Mat laneOrientationImage;

    mglGraph laneGraph;
    double y_max;
    bool graphIsConfigured;

    bool oriantationGraphScaleIsFrozen;

    int orientationBins;
    mglGraph orientationHistogram;
    std::vector<double> gradientOrientations;
    double averageOrientation;
    int orientationCount;

    std::array<cv::Point2f,4> inputQuad;
    std::array<cv::Point2f,4> outputQuad;
    bool shouldInitializePerspectiveFromScratch;
    bool shouldGuessQuadsFromLabda;

    void updateCurvature();
    void updateLMTThreshold(int value);

    void configureLaneGraph(cv::Mat mat, double max);

    std::vector<int> findBlobs(std::vector<double> histogram, int radius, double threshold);

    void addToGradientOrientations(int &blob, int radius);

    void updateLaneSilhouette(cv::Mat frame);
    void updateLaneMotionTemplate();

    cv::Mat previousLMT;

    void initialisePerspectiveFromFrame(cv::Mat frame);

    void guessQuadsFromLabda(cv::Mat frame);

    bool laneIntensityLogScale;

    void freezeOrientationScale();

};


#endif //SENCITY_MOTIONTEMPLATELANEDETECTION_H
