//
// Created by symeon on 18/06/18.
//

#ifndef SENCITY_PROCESSING_H
#define SENCITY_PROCESSING_H


#include <opencv2/core/mat.hpp>

class FrameProcessing {
public:
    virtual void process(cv::Mat frame) = 0;
    virtual void drawGUI() = 0;
};


#endif //SENCITY_PROCESSING_H
