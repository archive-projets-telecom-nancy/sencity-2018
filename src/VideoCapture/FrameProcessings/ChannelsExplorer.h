//
// Created by symeon on 29/06/18.
//

#ifndef SENCITY_CHANNELSEXPLORER_H
#define SENCITY_CHANNELSEXPLORER_H


#include <opencv2/opencv.hpp>
#include "FrameProcessing.h"

struct windowsStatus {
    bool shoudShow;
    bool hasShown;
};

class colorSpace {
public:
    colorSpace(std::string name, int conversionCode);
    colorSpace(std::string name, int conversionCode, std::vector<std::string> channelNames);
    std::string name;
    cv::Mat image;
    int conversionCode;
    bool shouldShow;
    bool hasShown;
    std::vector<std::tuple<std::string,windowsStatus>> channels;
};

class ChannelsExplorer : public FrameProcessing {
public:
    ChannelsExplorer();
    void process(cv::Mat frame) override;
    void drawGUI() override;

    cv::Mat interface;

    std::vector<colorSpace> colorSpaces;
};


#endif //SENCITY_CHANNELSEXPLORER_H
