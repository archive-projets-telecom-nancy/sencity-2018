//
// Created by symeon on 18/06/18.
//

#include <opencv2/opencv.hpp>
#include <algorithm>
#include <utility>
#include <opencv/ml.h>
#include <mgl2/mgl.h>
#include "MotionTemplateLaneDetection.h"
#include "../../Toolbox/LaplacianKernel.h"
#include "../../Toolbox/VariousTools.h"

MotionTemplateLaneDetection::MotionTemplateLaneDetection(int PsiThreshold, int delta):
        previousLMT(),
        D(),
        Psi(),
        laneSilhouette(),
        laneBeforeThreshold(),
        LMT(),
        PsiThreshold(PsiThreshold),
        lmtDecay(delta),
        laneGraph(),
        graphIsConfigured(false),
        orientationBins(9),
        gradientOrientations(orientationBins),
        orientationHistogram(),
        laneIntesityImage(),
        laneOrientationImage(),
        blobRadius(40),
        silouhetteThreshold(200),
        shouldThreshold(true),
        shouldInitializePerspectiveFromScratch(true),
        shouldGuessQuadsFromLabda(false),
        laneIntensityLogScale(false),
        shouldEqualizeHist(true),
        shouldBlur(false),
        blurRadius(21),
        oriantationGraphScaleIsFrozen(false),
        laneCannyEdgeDetection(),
        shouldDetectEdges(false),
        cannyUpperBound(100),
        cannyLowerBound(50)
{

    this->laplacianKernel = LaplacianKernel::horizontalLaplacianKernel(15);
    this->gradientOrientations.reserve(static_cast<unsigned long>(orientationBins));
    this->orientationHistogram.SetRange('x',0,this->orientationBins-1);

}

void MotionTemplateLaneDetection::process(cv::Mat frame) {
    if (shouldInitializePerspectiveFromScratch) {
        initialisePerspectiveFromFrame(frame);
        shouldInitializePerspectiveFromScratch = false;
    }
    if (shouldGuessQuadsFromLabda) {
        guessQuadsFromLabda(frame);
        shouldGuessQuadsFromLabda = false;
    }
    updateLaneSilhouette(std::move(frame));
    updateLaneMotionTemplate();
    updateCurvature();

}

void MotionTemplateLaneDetection::updateLaneSilhouette(cv::Mat frame) {

    cv::Mat output = cv::Mat(frame.rows, frame.cols, frame.type());
    cv::warpPerspective(frame, laneSilhouette, this->perspectiveMatrix, output.size());
    cv::cvtColor(laneSilhouette, laneSilhouette, cv::COLOR_BGR2GRAY);

    if (shouldEqualizeHist) {
        cv::equalizeHist(laneSilhouette, laneSilhouette);
    }

    if (shouldBlur) {
        cv::GaussianBlur(laneSilhouette,laneSilhouette,cv::Size(blurRadius,blurRadius),0);
    }

    if (shouldDetectEdges) {
        cv::Canny(laneSilhouette,laneSilhouette,cannyLowerBound,cannyUpperBound);
    }

    if (shouldThreshold) {
        cv::threshold(laneSilhouette, laneSilhouette, silouhetteThreshold, 255, cv::THRESH_TOZERO);
    }
    //cv::Sobel(laneSilhouette,laneSilhouette,-1,1,0);

}

void MotionTemplateLaneDetection::updateLMTThreshold(int value) {
    this->silouhetteThreshold = value;
    cv::threshold(laneBeforeThreshold,laneSilhouette,silouhetteThreshold,255,cv::THRESH_TOZERO);
}

void MotionTemplateLaneDetection::updateLaneMotionTemplate() {


    if (previousLMT.empty()) {
        previousLMT = cv::Mat::zeros(laneSilhouette.rows,laneSilhouette.cols,laneSilhouette.type());
    }

    cv::Mat resultat = cv::Mat::zeros(laneSilhouette.rows,laneSilhouette.cols,laneSilhouette.type());

    cv::MatIterator_<uchar> res_it, res_end, lmt_it, lmt_end, sil_it, sil_end;
    for (
            sil_it = laneSilhouette.begin<uchar>(),
            sil_end = laneSilhouette.end<uchar>(),
            res_it = resultat.begin<uchar>(),
            res_end = resultat.end<uchar>(),
            lmt_it = previousLMT.begin<uchar>(),
            lmt_end = previousLMT.end<uchar>();
            sil_it != sil_end and
            res_it != res_end and
            lmt_it != lmt_end;
            ++sil_it,
            ++res_it,
            ++lmt_it
    ) {
        if (static_cast<int>(*sil_it) > PsiThreshold) {
            *res_it = *sil_it;
        } else {
            *res_it = static_cast<uchar>(std::max(0, static_cast<int>(*lmt_it) - lmtDecay));
        }
    }
    previousLMT = resultat.clone();
    LMT = previousLMT.clone();
}

void MotionTemplateLaneDetection::updateCurvature() {

    cv::Mat intesityHistogram;
    cv::reduce(LMT,intesityHistogram,0,CV_REDUCE_SUM,CV_32FC1);
    //std::cout << intesityHistogram << std::endl;
    std::vector<double> histogram_;
    cv::MatIterator_<float> it, end;
    double max = 0;
    for (it = intesityHistogram.begin<float>(), end = intesityHistogram.end<float>(); it != end; ++it) {
        histogram_.push_back(this->laneIntensityLogScale ? log(*it + 1) : *it);
        if ((this->laneIntensityLogScale ? log(*it + 1) : *it) > max) {
            max = this->laneIntensityLogScale ? log(*it + 1) : *it;
        }
    }
    //std::cout << "max : " << max << std::endl;
    std::vector<int> blobs = findBlobs(histogram_, blobRadius, 1);

    ///*
    if (not graphIsConfigured) {
        configureLaneGraph(LMT, max);
    }
    mglData laneInstensityData(intesityHistogram.cols,1,1);
    laneInstensityData.Set(histogram_);
    this->laneGraph.ClearFrame();
    this->laneGraph.AddLegend("Lane Intensity",":rC");
    this->laneGraph.Plot(laneInstensityData,"r");
    this->laneGraph.Axis();
    //*/

    std::fill(gradientOrientations.begin(),gradientOrientations.begin()+gradientOrientations.capacity(),0);
    this->orientationCount = 1;
    this->averageOrientation = 0;
    for (int &blob : blobs) {
        ///*
        this->laneGraph.Puts(mglPoint(blob,histogram_[blob]*1.05,0),"\\downarrow");
        this->laneGraph.Line(mglPoint(blob-blobRadius,0,0),mglPoint(blob-blobRadius,y_max,0),"b=");
        this->laneGraph.Line(mglPoint(blob+blobRadius,0,0),mglPoint(blob+blobRadius,y_max,0),"b=");
        //*/
        addToGradientOrientations(blob, blobRadius);
    }
    ///*
    mglData laneOrientationData(this->orientationBins,1,1);
    laneOrientationData.Set(this->gradientOrientations);
    this->orientationHistogram.ClearFrame();
    if (not this->oriantationGraphScaleIsFrozen) {
        this->orientationHistogram.SetRange('y',laneOrientationData);
    }

    this->orientationHistogram.Bars(laneOrientationData,"B");
    this->orientationHistogram.Axis();

    laneIntesityImage = cv::Mat(laneGraph.GetHeight(),laneGraph.GetWidth(),CV_8UC3,(void*) laneGraph.GetRGB());
    cv::cvtColor(laneIntesityImage,laneIntesityImage,cv::COLOR_BGR2RGB);

    laneOrientationImage = cv::Mat(orientationHistogram.GetHeight(),orientationHistogram.GetWidth(),CV_8UC3,(void*) orientationHistogram.GetRGB());
    cv::cvtColor(laneOrientationImage,laneOrientationImage,cv::COLOR_BGR2RGB);
}

void MotionTemplateLaneDetection::configureLaneGraph(cv::Mat mat, double max) {
    y_max = max *1.2;
    laneGraph.SetRange('x',0,mat.cols-1);
    laneGraph.SetRange('y',0,y_max);
    graphIsConfigured = true;
}

std::vector<int> MotionTemplateLaneDetection::findBlobs(std::vector<double> histogram, int radius, double threshold) {
    std::vector<int> res;
    double max_value;
    int max_delta;
    int x;
    for (int i = 0; i < histogram.size(); ++i) {
        if (histogram[i] > threshold) {
            max_value = 0;
            max_delta = -1;
            for (int delta = -radius; delta < radius + 1; ++delta) {
                x = std::max(0, i + delta);
                x = std::min(x, (int) histogram.size());
                if (histogram[x] > max_value) {
                    max_value = histogram[x];
                    max_delta = delta;
                }
            }
            if (max_delta == 0) {
                res.push_back(i);
            }
        }
    }
    return res;
}

void MotionTemplateLaneDetection::addToGradientOrientations(int &blob, int radius) {

    int left_boundary = std::max(blob-radius,0);
    int right_boundary = std::min(this->LMT.cols-1,blob+radius);

    cv::Mat SobelX;
    cv::Mat SobelY;
    cv::Mat LMT_slice = LMT.colRange(left_boundary,right_boundary);
    cv::Sobel(LMT_slice,SobelX,CV_64F,0,1);
    cv::Sobel(LMT_slice,SobelY,CV_64F,1,0);

    cv::MatIterator_<double> x_it, x_end, y_it, y_end;
    cv::MatIterator_<uchar> lmt_it, lmt_end;
    for (
            x_it = SobelX.begin<double>(),
            x_end = SobelX.end<double>(),
            y_it = SobelY.begin<double>(),
            y_end = SobelY.end<double>(),
            lmt_it = LMT_slice.begin<uchar>(),
            lmt_end = LMT_slice.end<uchar>();
            x_it != x_end and
            y_it != y_end and
            lmt_it != lmt_end;
            ++x_it,
            ++y_it,
            ++lmt_it
    )
    {
        double res = atan2(*y_it,*x_it);
        auto quotient = static_cast<int>(res / (M_PI / this->orientationBins));
        averageOrientation = averageOrientation + (res - averageOrientation) / static_cast<double>(orientationCount);
        orientationCount++;
        this->gradientOrientations[quotient%orientationBins] += static_cast<int>(*lmt_it);
    }
}

void MotionTemplateLaneDetection::initialisePerspectiveFromFrame(cv::Mat frame) {
    inputQuad = {
            cv::Point2f(0, static_cast<float>(frame.rows / 2.0)),
            cv::Point2f(frame.cols-1, static_cast<float>(frame.rows / 2.0)),
            cv::Point2f(0,frame.rows-1),
            cv::Point2f(frame.cols-1,frame.rows-1)
    };

    outputQuad = {
            cv::Point2f(0,0),
            cv::Point2f(frame.cols-1,0),
            cv::Point2f(static_cast<float>(frame.cols * 3.5 / 8.0), frame.rows - 1),
            cv::Point2f(static_cast<float>(frame.cols * 4.5 / 8.0), frame.rows - 1)
    };
    perspectiveMatrix = cv::getPerspectiveTransform( inputQuad, outputQuad );
}

void MotionTemplateLaneDetection::withLambda(cv::Mat lambda) {
    assert(lambda.cols == 3);
    assert(lambda.rows == 3);
    this->perspectiveMatrix = lambda.clone();
    this->shouldGuessQuadsFromLabda = true;
    this->shouldInitializePerspectiveFromScratch = false;
}

void MotionTemplateLaneDetection::guessQuadsFromLabda(cv::Mat frame) {
    outputQuad = {
            cv::Point2f(0,0),
            cv::Point2f(frame.cols-1,0),
            cv::Point2f(static_cast<float>(frame.cols * 3 / 8.0), frame.rows - 1),
            cv::Point2f(static_cast<float>(frame.cols * 5 / 8.0), frame.rows - 1)
    };
    inputQuad = {
            cv::Point2f(0, static_cast<float>(frame.rows / 2.0)),
            cv::Point2f(frame.cols-1, static_cast<float>(frame.rows / 2.0)),
            cv::Point2f(0,frame.rows-1),
            cv::Point2f(frame.cols-1,frame.rows-1)
    };
    cv::perspectiveTransform(outputQuad,inputQuad,perspectiveMatrix.inv());
}

void MotionTemplateLaneDetection::freezeOrientationScale() {
    this->oriantationGraphScaleIsFrozen = true;
    this->orientationHistogram.SetRange('y',0,*std::max_element(gradientOrientations.begin(),gradientOrientations.end())*1.2);
}
