//
// Created by symeon on 29/06/18.
//

#include "ChannelsExplorer.h"
#include <cstdio>
#include <utility>
#include "../../Toolbox/cvui.h"

ChannelsExplorer::ChannelsExplorer():
    interface(600,200,CV_8UC3)
{
    colorSpaces.push_back(colorSpace("RGB",cv::COLOR_BGR2RGB));
    colorSpaces.push_back(colorSpace("HSV",cv::COLOR_BGR2HSV));
    colorSpaces.push_back(colorSpace("HLS",cv::COLOR_BGR2HLS));
    colorSpaces.push_back(colorSpace("CIE L*a*b*",cv::COLOR_BGR2Lab,{"L*","a*","b*"}));
    colorSpaces.push_back(colorSpace("CIE L*u*v*",cv::COLOR_BGR2Luv,{"L*","u*","v*"}));
    colorSpaces.push_back(colorSpace("YCrCb JPEG",cv::COLOR_BGR2YCrCb,{"Y","Cr","Cb"}));
    cvui::init("Channels Explorer");
    cvui::watch("Channels Explorer");
}

void ChannelsExplorer::process(cv::Mat frame) {
    for (auto& colorspace : colorSpaces) {
        if (colorspace.shouldShow) {
            cv::cvtColor(frame,colorspace.image,colorspace.conversionCode);
            for (int i = 0; i < colorspace.channels.size(); ++i) {
                windowsStatus& channelStatus = std::get<1>(colorspace.channels[i]);
                if (channelStatus.shoudShow) {
                    cv::Mat channel;
                    cv::extractChannel(colorspace.image,channel,i);
                    cv::imshow(colorspace.name+" : "+std::get<0>(colorspace.channels[i]),channel);
                    channelStatus.hasShown = true;
                } else {
                    if (channelStatus.hasShown) {
                        cv::destroyWindow(colorspace.name + " : " + std::get<0>(colorspace.channels[i]));
                        channelStatus.hasShown = false;
                    }
                }
            }
        } else {
            for (int i = 0; i < colorspace.channels.size(); ++i) {
                if (std::get<1>(colorspace.channels[i]).hasShown) {
                    cv::destroyWindow(colorspace.name+" : "+std::get<0>(colorspace.channels[i]));
                    std::get<1>(colorspace.channels[i]).hasShown = false;
                }
            }
        }
    }
}

void ChannelsExplorer::drawGUI() {

    cvui::context("Channels Explorer");
    interface = cv::Scalar(49, 52, 49);

    cvui::beginColumn(interface, 5, 5, -1, -1, 10);
    {
        for (auto &colorspace : colorSpaces) {
            if(cvui::checkbox(colorspace.name,&colorspace.shouldShow)) {
                cvui::beginRow(-1,-1,2);
                {
                    cvui::rect(15,0,0xff000000);
                    for (auto &channel : colorspace.channels) {
                        cvui::checkbox(std::get<0>(channel), &(std::get<1>(channel).shoudShow));
                    }
                }
                cvui::endRow();
            }
            cvui::rect(290, 1, 0x707070);
        }
    }
    cvui::endColumn();
    cvui::update("Channels Explorer");

    cvui::imshow("Channels Explorer",interface);

}

colorSpace::colorSpace(std::string name, int conversionCode):
        name(name),
        image(cv::Mat().clone()),
        conversionCode(conversionCode),
        shouldShow(false),
        hasShown(false)
{
    for (auto& letter: name) {
        this->channels.push_back({std::string(1,letter),{false, false}});
    }
}

colorSpace::colorSpace(std::string name, int conversionCode, std::vector<std::string> channelNames):
        name(std::move(name)),
        image(cv::Mat().clone()),
        conversionCode(conversionCode),
        shouldShow(false),
        hasShown(false)
{
    for (auto& channelName: channelNames) {
        this->channels.push_back({channelName,{false, false}});
    }
}
