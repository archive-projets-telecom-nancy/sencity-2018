//
// Created by symeon on 09/07/18.
//

#include "FramePipeComposition.h"
#include "../../Toolbox/cvui.h"

#define INTERFACE_NAME "Frame Pipeline"

FramePipeComposition::FramePipeComposition():
        cp(Composition()),
        interface(600,500,CV_8UC3)
{
    cvui::init(INTERFACE_NAME);
    cvui::watch(INTERFACE_NAME);
}

void FramePipeComposition::process(cv::Mat frame) {
    cvui::imshow("Output",cp.process(frame));
}

void FramePipeComposition::drawGUI() {
    cvui::context(INTERFACE_NAME);
    if (interface.empty()) {
        interface = cv::Mat(600,500,CV_8UC3);
    }
    interface.setTo(cv::Scalar(49, 52, 49));

    cvui::beginColumn(interface, 5, 5, -1, -1, 2);
    {
        cp.drawGUI();
    }
    cvui::endColumn();

    cvui::update(INTERFACE_NAME);
    cvui::imshow(INTERFACE_NAME,interface);
}
