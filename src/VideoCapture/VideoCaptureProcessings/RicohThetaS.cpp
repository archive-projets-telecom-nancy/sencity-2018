//
// Created by symeon on 13/06/18.
//

#include <opencv2/opencv.hpp>
#include "RicohThetaS.h"
#include "../../Transforms/EquirectangularProjection.h"
#include "../FrameProcessings/FramePipes/PerspectiveProjection.h"

void RicohThetaS::process(cv::VideoCapture& cap) {

    cv::namedWindow("Input",1);
    cv::namedWindow("Equirectuangular Transform", 1);
    cv::namedWindow("Perspective Transform", 1);

    cv::Mat input_frame;
    cv::Mat equirectangular_frame;
    cv::Mat perspective_frame;


    EquirectangularProjection ef = EquirectangularProjection(1280,640,37);
    PerspectiveProjection pf = PerspectiveProjection(1280,720,100);

    int key;
    bool ZoneASelected = true;
    cv::Scalar selectedColor(0,0,255);
    cv::Scalar defaultColor(255,225,127);
    for(;;) {

        cap >> input_frame;
        equirectangular_frame = ef.warp(input_frame);
        perspective_frame = pf.warp(input_frame);
        if (ZoneASelected) {
            cv::ellipse(input_frame,ef.getRotatedRectA(),selectedColor);
            cv::ellipse(input_frame,ef.getRotatedRectB(),defaultColor);
        } else {
            cv::ellipse(input_frame,ef.getRotatedRectA(),defaultColor);
            cv::ellipse(input_frame,ef.getRotatedRectB(),selectedColor);
        }

        imshow("Input", input_frame);
        imshow("Equirectuangular Transform",equirectangular_frame);
        imshow("Perspective Transform",perspective_frame);
        if ((key = cv::waitKey(1)) >= 0) {
            switch (key) {
                case 13: // ENTER
                    ef.updateMaps();
                    break;
                case 27: // ECHAP
                    std::cout << "ef : \n" << ef << std::endl;
                    return;
                case 81: // GAUCHE
                    ef.move(-1,0,ZoneASelected);
                    break;
                case 82: // HAUT
                    ef.move(0,-1,ZoneASelected);
                    break;
                case 83: // DROITE
                    ef.move(1,0,ZoneASelected);
                    break;
                case 84: // BAS
                    ef.move(0,1,ZoneASelected);
                    break;
                case 97: // A
                    ZoneASelected = true;
                    break;
                case 98: // B
                    ZoneASelected = false;
                    break;
                case 171: // NUMPAD +
                    ef.updateCrop(-1,ZoneASelected);
                    break;
                case 173: // NUMPAD -
                    ef.updateCrop(1,ZoneASelected);
                    break;
                case 178: // NUMPAD 2 (down)
                    ef.pinchY(-1,ZoneASelected);
                    break;
                case 180: // NUMPAD 4 (left)
                    ef.pinchX(1,ZoneASelected);
                    break;
                case 182: // NUMPAD 6 (right)
                    ef.pinchX(-1,ZoneASelected);
                    break;
                case 184: // NUMPAD 8 (up)
                    ef.pinchY(1,ZoneASelected);
                    break;
                default:
                    std::cout << "key pressed :" << key << std::endl;
            }
        }
    }

}
