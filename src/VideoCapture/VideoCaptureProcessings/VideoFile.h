//
// Created by symeon on 13/06/18.
//

#ifndef SENCITY_VIDEOFILE_H
#define SENCITY_VIDEOFILE_H


#include "VideoCaptureProcessing.h"
#include "../FrameProcessings/MotionTemplateLaneDetection.h"

class VideoFile : public VideoCaptureProcessing {
public:
    explicit VideoFile(FrameProcessing& fp);
    void process(cv::VideoCapture &cap) override;

    FrameProcessing& fp;

    void drawGUI();

    cv::Mat inputFrame;
    cv::Mat interface;

    bool getNextFrame;
    bool keepOnPlaying;
};


#endif //SENCITY_VIDEOFILE_H
