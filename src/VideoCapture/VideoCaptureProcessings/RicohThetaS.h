//
// Created by symeon on 13/06/18.
//

#ifndef SENCITY_THETASTREATEMENT_H
#define SENCITY_THETASTREATEMENT_H


#include <opencv2/videoio.hpp>
#include "VideoCaptureProcessing.h"

class RicohThetaS : public VideoCaptureProcessing {
public:
    void process(cv::VideoCapture &cap) override;
};


#endif //SENCITY_THETASTREATEMENT_H
