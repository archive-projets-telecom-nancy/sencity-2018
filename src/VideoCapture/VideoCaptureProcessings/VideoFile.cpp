//
// Created by symeon on 13/06/18.
//

#include <opencv2/opencv.hpp>
#include <iostream>
#include "VideoFile.h"
#include "../../Toolbox/cvui.h"

VideoFile::VideoFile(FrameProcessing& fp):
        fp(fp),
        interface(55,300,CV_8UC3),
        keepOnPlaying(false),
        getNextFrame(true)
{}

void VideoFile::process(cv::VideoCapture &cap) {

    cv::namedWindow("Input",1);

    cv::moveWindow("Input",20,20);

    inputFrame = cv::Mat();

    cvui::init("Playback Controls");
    cv::moveWindow("Playback Controls",20+700*2,20);

    int key;
    bool exit = false;

    while(!exit) {

        if ((key = cv::waitKey(1)) >= 0) {
            switch (key) {
                case 27: // ECHAP
                    exit = true;
                    break;
                default:
                    std::cout << "key pressed :" << key << std::endl;
                    break;
            }
        }

        if (getNextFrame or keepOnPlaying) {
            getNextFrame = false;
            cap >> inputFrame;
        }

        fp.process(inputFrame);
        cv::imshow("Input",inputFrame);
        drawGUI();
        fp.drawGUI();
        cvui::imshow("Playback Controls", interface);

    }
}

void VideoFile::drawGUI() {

    cvui::context("Playback Controls");
    // Fill the frame with a nice color
    interface = cv::Scalar(49, 52, 49);

    // Render UI components to the frame
    cvui::beginColumn(interface, 5, 5, -1, -1, 10);

    cvui::text("Playback Controls", 0.4, 0xEAD97E);
    {
        cvui::beginRow(-1, -1, 2);
        if (keepOnPlaying) {
            if (cvui::button("PAUSE")) {
                keepOnPlaying = false;
            }
        } else {
            if (cvui::button("PLAY")) {
                keepOnPlaying = true;
            }
        }
        if (cvui::button("Frame by Frame")) {
            getNextFrame = true;
        }
        cvui::endRow();
    }
    cvui::endColumn();

    cvui::update("Playback Controls");
}


