//
// Created by symeon on 13/06/18.
//

#ifndef SENCITY_ABSTRACTTREATEMENT_H
#define SENCITY_ABSTRACTTREATEMENT_H


#include <opencv2/videoio.hpp>

class VideoCaptureProcessing {
public:
    virtual void process(cv::VideoCapture& cap) = 0;
};


#endif //SENCITY_ABSTRACTTREATEMENT_H
