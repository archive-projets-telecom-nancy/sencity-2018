//
// Created by symeon on 24/07/18.
//

#ifndef SENCITY_AUTORICOHTHETA_H
#define SENCITY_AUTORICOHTHETA_H


#include "VideoCaptureProcessing.h"
#include "../FrameProcessings/FramePipes/Composition.h"

class AutoRicohTheta : public VideoCaptureProcessing {
public:
    AutoRicohTheta();

    void process(cv::VideoCapture &cap) override;
    void drawGUI();

    Composition composition;
    cv::Mat interface;
};


#endif //SENCITY_AUTORICOHTHETA_H
