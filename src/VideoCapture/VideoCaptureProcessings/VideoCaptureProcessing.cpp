//
// Created by symeon on 13/06/18.
//

#include <opencv2/opencv.hpp>

#include "VideoCaptureProcessing.h"

void VideoCaptureProcessing::process(cv::VideoCapture& cap) {

    cv::namedWindow("Input",1);
    cv::Mat input_frame;

    int key;
    while(true) {
        cap >> input_frame;
        if (!input_frame.empty()) {
            cv::imshow("Input", input_frame);
        }
        if ((key = cv::waitKey(1)) >= 0) {
            switch (key) {
                case 27: // ECHAP
                    return;
                default:
                    std::cout << "key pressed :" << key << std::endl;
            }
        }
    }
}
