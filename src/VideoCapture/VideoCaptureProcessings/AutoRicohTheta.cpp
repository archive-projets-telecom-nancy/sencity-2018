//
// Created by symeon on 24/07/18.
//

#include "AutoRicohTheta.h"
#include "../FrameProcessings/FramePipes/Mixer.h"
#include "../FrameProcessings/FramePipes/ReversiblePerspective.h"
#include "../FrameProcessings/FramePipes/EqualizeHistogram.h"
#include "../FrameProcessings/FramePipes/Threshold.h"
#include "../FrameProcessings/FramePipes/RossKippenbrockLaneDetection.h"
#include "../../Toolbox/cvui.h"
#include "../FrameProcessings/FramePipes/PerspectiveProjection.h"
#include "../FrameProcessings/FramePipes/ChannelSelector.h"
#include "../FrameProcessings/FramePipes/CannyEdgeDetection.h"
#include "../FrameProcessings/FramePipes/ColorspaceConversion.h"
#include "../FrameProcessings/FrameMixers/Max.h"

#define INTERFACE_NAME "Frame Pipeline"

AutoRicohTheta::AutoRicohTheta():
    composition(),
    interface(600,500,CV_8UC3)
{
    cvui::init(INTERFACE_NAME);
    cvui::watch(INTERFACE_NAME);
    composition.steps.emplace_back("Projection");
    composition.steps.emplace_back("Mixer");
    composition.steps[0].fp = std::make_unique<PerspectiveProjection>();
    composition.steps[1].fp = std::make_unique<Mixer>();
    composition.numberOfSteps = 2;

    Composition& leftMixerComp = dynamic_cast<Mixer*>(composition.steps[1].fp.get())->A;
    dynamic_cast<Mixer*>(composition.steps[1].fp.get())->frameMixer = {"Max",std::make_unique<Max>()};
    leftMixerComp.steps.emplace_back("Perspective");
    leftMixerComp.steps[0].fp = std::make_unique<ReversiblePerspective>();
    leftMixerComp.numberOfSteps = 1;

    Composition& innerPerspectiveComp = dynamic_cast<ReversiblePerspective*>(leftMixerComp.steps[0].fp.get())->composition;
    innerPerspectiveComp.steps.emplace_back("Equalize");
    innerPerspectiveComp.steps.emplace_back("Threshold");
    innerPerspectiveComp.steps.emplace_back("Lane Detection");
    innerPerspectiveComp.steps[0].fp = std::make_unique<EqualizeHistogram>();
    innerPerspectiveComp.steps[1].fp = std::make_unique<Threshold>(240);
    innerPerspectiveComp.steps[2].fp = std::make_unique<RossKippenbrockLaneDetection>();
    innerPerspectiveComp.numberOfSteps = 3;
}

void AutoRicohTheta::process(cv::VideoCapture &cap) {

    cv::namedWindow("Input",1);

    cv::moveWindow("Input",20,20);

    cv::Mat inputFrame = cv::Mat();

    int key;
    bool exit = false;

    while(!exit) {

        if ((key = cv::waitKey(1)) >= 0) {
            switch (key) {
                case 27: // ECHAP
                    exit = true;
                    break;
                default:
                    std::cout << "key pressed :" << key << std::endl;
                    break;
            }
        }

        cap >> inputFrame;

        cvui::imshow("Output",composition.process(inputFrame));
        cv::imshow("Input",inputFrame);
        this->drawGUI();
    }
}

void AutoRicohTheta::drawGUI() {
    cvui::context(INTERFACE_NAME);
    if (interface.empty()) {
        interface = cv::Mat(600,500,CV_8UC3);
    }
    interface.setTo(cv::Scalar(49, 52, 49));

    cvui::beginColumn(interface, 5, 5, -1, -1, 2);
    {
        composition.drawGUI();
    }
    cvui::endColumn();

    cvui::update(INTERFACE_NAME);
    cvui::imshow(INTERFACE_NAME,interface);
}
