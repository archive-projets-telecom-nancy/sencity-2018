//
// Created by symeon on 06/06/18.
//

#include <stdexcept>
#include "AffineTransform.h"

AffineTransform::AffineTransform(double low_input, double high_input, double low_output, double high_output) {
    if (low_input == high_input) {
        throw std::invalid_argument("low and high input values for affine transform must be different !");
    }
    a = (high_output-low_output)/(high_input-low_input);
    b = (high_input*low_output - high_output*low_input)/(high_input-low_input);
}

double AffineTransform::transform(double val) {
    return a*val + b;
}

AffineTransform::AffineTransform(double coeff, double offset) {
    a = coeff;
    b = offset;
}

double AffineTransform::getCoeff() {
    return a;
}

AffineTransform AffineTransform::matching(double input, double output) {
    return AffineTransform(input, input+1, output, output+a);
}

AffineTransform::AffineTransform(double coeff) {
    a = coeff;
    b = 0;
}
