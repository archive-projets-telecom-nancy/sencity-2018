//
// Created by symeon on 06/06/18.
//

#ifndef SENCITY_EQUIRECTANGULARTRANSFORM_H
#define SENCITY_EQUIRECTANGULARTRANSFORM_H

#include <opencv2/opencv.hpp>
#include <cstdio>
#include <ostream>

class EquirectangularProjection {
public:
    explicit EquirectangularProjection(int width=1280, int height=640, int crop=37);
    EquirectangularProjection(cv::Rect zoneA, cv::Rect zoneB, double ARoll, double BRoll, int output_height=640,
                                 int output_width=1280);
    cv::Mat warp(cv::Mat input_image);
    cv::RotatedRect getRotatedRectA();
    cv::RotatedRect getRotatedRectB();
    void updateMaps();
    void updateCrop(int amount, bool zoneA);
    void move(int x, int y, bool zoneA);
    void pinchX(int amount, bool zoneA);
    void pinchY(int amount, bool zoneA);
    void updateFacteurInflexionRayon(double amount);
    void updateARoll(double amount);
    void updateBRoll(double amount);

    friend std::ostream &operator<<(std::ostream &os, const EquirectangularProjection &transform);

private:

    int output_height;
    int output_width;
    cv::Rect input_zone_A;
    cv::Rect input_zone_B;
    double zoneARoll;
    double zoneBRoll;
    double facteurInflexionRayon;
    cv::Mat map_x;
    cv::Mat map_y;

    void updateCrop(int amount, cv::Rect& target);
    void move(int x, int y, cv::Rect& target);
    void pinchX(int amount, cv::Rect& target);
    void pinchY(int amount, cv::Rect& target);
    cv::RotatedRect getRotatedRectFromRect(cv::Rect input_rect);
    double inflexionRayon(double rayon);
};

typedef struct double2 {
    double x;
    double y;
} double2;


#endif //SENCITY_EQUIRECTANGULARTRANSFORM_H
