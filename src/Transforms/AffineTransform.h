//
// Created by symeon on 06/06/18.
//

#ifndef SENCITY_AFFINETRANSFORM_H
#define SENCITY_AFFINETRANSFORM_H


class AffineTransform {
public:
    AffineTransform(double low_input, double high_input, double low_output, double high_output);
    AffineTransform(double coeff, double offset);
    AffineTransform(double coeff);
    AffineTransform matching(double input, double output);
    double transform(double val);
    double getCoeff();
private:
    double a;
    double b;
};


#endif //SENCITY_AFFINETRANSFORM_H
