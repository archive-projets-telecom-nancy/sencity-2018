//
// Created by symeon on 06/06/18.
//

#include "EquirectangularProjection.h"
#include "AffineTransform.h"
#include <cmath>


EquirectangularProjection::EquirectangularProjection(int width, int height, int crop) {

    this->facteurInflexionRayon = 0;
    this->zoneARoll = M_PI/2.0;
    this->zoneBRoll = -M_PI/2.0;
    this->input_zone_A = cv::Rect(crop,crop,height-2*crop,height-2*crop);
    this->input_zone_B = cv::Rect(width/2+crop,crop,height-2*crop,height-2*crop);
    output_width = width;
    output_height = height;
    updateMaps();
}

EquirectangularProjection::EquirectangularProjection(cv::Rect zoneA, cv::Rect zoneB, double ARoll, double BRoll, int output_height,
                                                   int output_width) {
    this->output_height = output_height;
    this->output_width = output_width;
    this->input_zone_A = zoneA;
    this->input_zone_B = zoneB;
    this->zoneARoll = ARoll;
    this->zoneBRoll = BRoll;
    updateMaps();
}

cv::Mat EquirectangularProjection::warp(cv::Mat input_image) {

    cv::Mat output_image = cv::Mat(output_height,output_width,CV_8UC3,cv::Scalar(0,0,0));

    remap( input_image, output_image, map_x, map_y, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0) );

    return output_image.clone();
}

void EquirectangularProjection::updateMaps() {

    map_x.create(output_height,output_width,cv::DataType<float>::type);
    map_y.create(output_height,output_width,cv::DataType<float>::type);

    AffineTransform uToTheta(0,output_width-1,2*M_PI,0);
    AffineTransform vToPhi(0,output_height-1,0,M_PI);

    AffineTransform PxToInputX_A(-1,1,input_zone_A.x,input_zone_A.x+input_zone_A.width);
    AffineTransform PzToInputY_A(-1,1,input_zone_A.y+input_zone_A.height,input_zone_A.y);

    AffineTransform PxToInputX_B(-1,1,input_zone_B.x,input_zone_B.x+input_zone_B.width);
    AffineTransform PzToInputY_B(-1,1,input_zone_B.y+input_zone_B.height,input_zone_B.y);

    double theta,phi;
    double Px,Pz; //Py ne me sert à rien
    double rayon,angle;
    double xA,yA;
    double xB,yB;

    for (int u = 0; u < output_width/2; ++u) {
        for (int v = 0; v < output_height; ++v) {

            theta = uToTheta.transform(u);
            phi = vToPhi.transform(v);

            Px = sin(phi)*cos(theta) * -1.0; //je sais pas pourquoi il faut inverser au final ...
            //Py = sin(phi)*sin(theta);
            Pz = cos(phi);

            rayon = inflexionRayon(sqrt(Px*Px+Pz*Pz));
            angle = atan2(Pz,Px);

            xA = rayon*cos(angle+zoneARoll);
            yA = rayon*sin(angle+zoneARoll);

            xB = rayon*cos(angle+zoneBRoll);
            yB = rayon*sin(angle+zoneBRoll);

            map_x.at<float>(v,u) = static_cast<float>(PxToInputX_A.transform(xA));
            map_y.at<float>(v,u) = static_cast<float>(PzToInputY_A.transform(yA));
            map_x.at<float>(v,u+output_width/2) = static_cast<float>(PxToInputX_B.transform(xB));
            map_y.at<float>(v,u+output_width/2) = static_cast<float>(PzToInputY_B.transform(yB));

        }
    }

}

cv::RotatedRect EquirectangularProjection::getRotatedRectFromRect(cv::Rect input_rect) {
    cv::Point2f center(input_rect.x+input_rect.width/2,input_rect.y+input_rect.height/2);
    cv::Size2f side_lengths(input_rect.width,input_rect.height);
    return cv::RotatedRect(center,side_lengths,0);
}

cv::RotatedRect EquirectangularProjection::getRotatedRectA() {
    return getRotatedRectFromRect(input_zone_A);
}

cv::RotatedRect EquirectangularProjection::getRotatedRectB() {
    return getRotatedRectFromRect(input_zone_B);
}

void EquirectangularProjection::updateARoll(double amount) {
    this->zoneARoll += amount;
    updateMaps();
}

void EquirectangularProjection::updateBRoll(double amount) {
    this->zoneBRoll += amount;
    updateMaps();
}

double EquirectangularProjection::inflexionRayon(double rayon) {
    return 1-pow(1-pow(rayon,pow(2,facteurInflexionRayon)),pow(2,-facteurInflexionRayon));
}

void EquirectangularProjection::updateFacteurInflexionRayon(double amount) {
    this->facteurInflexionRayon += amount;
    updateMaps();
}

std::ostream &operator<<(std::ostream &os, const EquirectangularProjection &transform) {
    os << "input_zone_A: " << transform.input_zone_A << " input_zone_B: " << transform.input_zone_B << " zoneARoll: "
       << transform.zoneARoll << " zoneBRoll: " << transform.zoneBRoll << " facteurInflexionRayon: "
       << transform.facteurInflexionRayon;
    return os;
}

void EquirectangularProjection::updateCrop(int amount, bool zoneA) {
    if (zoneA) {
        updateCrop(amount,this->input_zone_A);
    } else {
        updateCrop(amount,this->input_zone_B);
    }
}

void EquirectangularProjection::updateCrop(int amount, cv::Rect& target) {
    target.x += amount;
    target.y += amount;
    target.width -= amount;
    target.height -= amount;
}

void EquirectangularProjection::move(int x, int y, cv::Rect& target) {
    target.x += x;
    target.y += y;
}

void EquirectangularProjection::move(int x, int y, bool zoneA) {
    if (zoneA) {
        move(x,y,this->input_zone_A);
    } else {
        move(x,y,this->input_zone_B);
    }
}

void EquirectangularProjection::pinchX(int amount, cv::Rect &target) {
    target.width -= amount;
}

void EquirectangularProjection::pinchY(int amount, cv::Rect &target) {
    target.height -= amount;
}

void EquirectangularProjection::pinchX(int amount, bool zoneA) {
    if (zoneA) {
        pinchX(amount,input_zone_A);
    } else {
        pinchX(amount,input_zone_B);
    }
}

void EquirectangularProjection::pinchY(int amount, bool zoneA) {
    if (zoneA) {
        pinchY(amount,input_zone_A);
    } else {
        pinchY(amount,input_zone_B);
    }
}




